package com.qcrud.spring.boot.example.mapper;

import com.qcrud.mapper.QcrudMapper;
import com.qcrud.spring.boot.example.entity.User;

public interface UserMapper extends QcrudMapper<User, Long> {

}
