package com.qcrud.spring.boot.example;

import com.qcrud.spring.boot.example.entity.User;
import com.qcrud.spring.boot.example.mapper.UserMapper;
import com.qcrud.tookit.SnowFlake;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/user")
public class UserController {
    @Resource
    private UserMapper userMapper;

    // 测试地址 http://localhost:8000/user/test
    @GetMapping("test")
    public User test() {
        User user = new User();
        user.setId(SnowFlake.nextId());
        user.setUsername("小羊肖恩");
        user.setSex(1);
        user.setStatus(1);
        Integer result = userMapper.insert(user);
        System.out.println(result);
        System.out.println(user.getId());
        return userMapper.selectById(user.getId()).get();
    }
}
