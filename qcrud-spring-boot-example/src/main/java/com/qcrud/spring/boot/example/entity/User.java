package com.qcrud.spring.boot.example.entity;

import com.qcrud.annotation.Column;
import com.qcrud.annotation.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Table("sys_user")
public class User {
    private Long id;
    private String username;
    private Integer sex;

    @Column(ignore = true)
    private String sexText;

    private Integer status;

    @Column(ignore = true)
    private StatusEnum statusEnum;

}
