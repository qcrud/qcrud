package com.qcrud.spring.boot.example;

import com.qcrud.spring.EnableQcrud;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableQcrud(basePackages = {
    "com.qcrud.spring.boot.example.mapper"
})
@SpringBootApplication
public class QcrudApplication {

    public static void main(String[] args) {
        try {
            SpringApplication.run(QcrudApplication.class, args);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
}

