package com.qcrud.annotation;

import java.lang.annotation.*;

@Inherited
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
public @interface Table {

    /**
     * 表名
     */
    String value();

    /**
     * 数据库 schema
     */
    String schema() default "";
}
