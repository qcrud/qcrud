package com.qcrud.annotation;

import java.lang.annotation.*;

@Inherited
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE})
public @interface Column {

    /**
     * 字段值，默认空使用属性名
     */
    String value() default "";

    /**
     * 非表字段忽略，默认 false
     */
    boolean ignore() default false;
}
