package com.qcrud.annotation;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface Param {

    String value();

    /**
     * 参数常量
     */
    String ID = "id";
    String IDS = "ids";
    String ET = "et";
    String ETS = "ets";
}
