<div style="text-align: center;">
    <a href="https://gitee.com/qcrud/qcrud">
        <img src="https://foruda.gitee.com/images/1662805256710152504/57745cf2_12260.png" width="100" height="100"  alt="qcrud-logo"/>
    </a>
    <p><strong>QCRUD</strong> - Quick! Quaint! Quality!</p>
</div>

## 介绍

QCRUD：新一代快速、有趣、优质的 ORM 框架，通过它你可以 快速(Quick) 增加(Create) 读取(Read) 更新(Update) 删除(Delete)。

## 开发环境

基于 JDK17 研发

- [各种JDK下载地址](https://www.injdk.cn/)

## SQL 标签

- ${sql.in("AND id", ids)}
> 等价于 `AND id IN (1,2,3)`

- ${sql.like("%字段", 内容)}
> 左匹配 `%字段` 右匹配 `字段%` 左右匹配 `%字段%` 或者 `字段`


## 核心依赖

- [jte](https://github.com/casid/jte) 高性能模板引擎

### JTE 模板引擎语法

> @import 导入依赖类 , @param 声明变量
```xml
@import my.Model
@param Model model

Hello ${model.name}!
```

```java
package my;
public class Model {
    public String name = "jte";
}
```

输出结果：`Hello jte`

- if 判断

```xml
@if(model.entries.isEmpty())
    I have no entries!
@elseif(model.entries.size() == 1)
    I have one entry!
@else
    I have ${model.entries.size()} entries!
@endif
```

- for 循环


```xml
@for(Entry entry : model.entries)
    <li>${entry.title}</li>
@endfor

@for(var entry : model.entries)
    <li>${entry.title}</li>
@endfor

@for(int i = 0; i < 10; ++i)
    <li>i is ${i}</li>
@endfor
```

- var 变量

```xml
!{var innerObject = someObject.object();}

${innerObject.a()}
${innerObject.b()}
```

### 常见问题

> 异常 module java.base does not "opens java.lang" 解决
> - 添加 VM 参数 `--add-opens=java.base/java.lang=ALL-UNNAMED`
