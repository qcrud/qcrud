package com.qcrud.mapper;

import com.qcrud.Qcrud;
import com.qcrud.annotation.*;
import com.qcrud.core.SqlCommandType;
import com.qcrud.core.SqlData;
import com.qcrud.core.SqlManager;
import com.qcrud.core.SqlMeta;
import com.qcrud.core.paging.Paging;
import com.qcrud.exception.QcrudException;
import com.qcrud.tookit.ConstantPool;
import lombok.extern.slf4j.Slf4j;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public record MapperMethod(Qcrud qcrud, Class mapperInterface, Method method) {

    public Object execute(Object[] args) {
        String methodName = method.getName();
        if (log.isDebugEnabled()) {
            log.debug("execute mapper method: " + methodName);
        }
        String statementKey = mapperInterface.getName() + "." + methodName;
        SqlMeta sqlMeta = SqlManager.get(statementKey);
        if (null == sqlMeta) {
            Annotation[] annotations = method.getAnnotations();
            for (Annotation annotation : annotations) {
                if (annotation instanceof Insert insert) {
                    sqlMeta = new SqlMeta(SqlCommandType.INSERT, insert.value(), method.getGenericReturnType());
                } else if (annotation instanceof Delete sqlDelete) {
                    sqlMeta = new SqlMeta(SqlCommandType.DELETE, sqlDelete.value(), method.getGenericReturnType());
                } else if (annotation instanceof Update sqlUpdate) {
                    sqlMeta = new SqlMeta(SqlCommandType.UPDATE, sqlUpdate.value(), method.getGenericReturnType());
                } else if (annotation instanceof Select select) {
                    sqlMeta = new SqlMeta(SqlCommandType.SELECT, select.value(), method.getGenericReturnType());
                } else if (annotation instanceof SqlCall sqlCall) {
                    sqlMeta = new SqlMeta(SqlCommandType.SELECT, sqlCall.value(), method.getGenericReturnType());
                }
            }
            if (null == sqlMeta) {
                throw new QcrudException("Cannot execute method，statementKey: " + statementKey);
            }
            SqlManager.put(statementKey, sqlMeta);
        }

        // md sql 补充返回类型
        if (null == sqlMeta.getReturnType()) {
            sqlMeta.setReturnType(method.getGenericReturnType());
        }

        // 执行 SQL 数据信息
        SqlData sqlData = new SqlData(sqlMeta, statementKey, SqlManager.getGenericEntityClass(mapperInterface));
        int argSize = null == args ? 0 : args.length;
        if (argSize > 0) {
            int i = 0;
            Map<String, Object> params = new HashMap<>();
            List<Param> paramList = SqlManager.getMethodParams(statementKey, method);
            if (SqlCommandType.SELECT == sqlMeta.getSqlCommandType()) {
                // 查询语句判断是否为分页
                if (argSize != paramList.size()) {
                    if (args[0] instanceof Paging) {
                        // 索引跳过分页参数
                        i++;
                    } else {
                        // 参数不匹配且第一位非分页参数对象抛出异常
                        throw new QcrudException("execute mapper method: " + methodName + ", each parameter must be annotated with @SqlParam ");
                    }
                }
                if (i > 0) {
                    params.put(ConstantPool.PAGING_KEY, args[0]);
                }
            }
            for (Param param : paramList) {
                params.put(param.value(), args[i++]);
            }
            sqlData.setParams(params);
        }
        return qcrud.execute(sqlData);
    }
}

