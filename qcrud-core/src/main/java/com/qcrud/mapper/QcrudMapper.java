package com.qcrud.mapper;

import com.qcrud.annotation.*;

import java.util.List;
import java.util.Optional;

public interface QcrudMapper<T, ID> {

    /**
     * 插入
     *
     * @param entity 插入实体
     * @return
     */
    @Insert("${sql.insert(et)}")
    int insert(@Param(Param.ET) T entity);

    /**
     * 批量插入
     *
     * @param entityList 插入实体列表
     * @return
     */
    @Insert("${sql.insertBatch(ets)}")
    int insertBatch(@Param(Param.ETS) List<T> entityList);

    /**
     * 根据 主键ID 删除
     *
     * @param id 主键ID
     * @return
     */
    @Delete("${sql.deleteById(id)}")
    int deleteById(@Param(Param.ID) ID id);

    /**
     * 根据 主键ID 批量删除
     *
     * @param ids 主键ID列表
     * @return
     */
    @Delete("${sql.deleteBatchByIds(ids)}")
    int deleteBatchByIds(@Param(Param.IDS) List<ID> ids);

    /**
     * 根据 主键ID 更新
     *
     * @param entity 更新实体
     * @return
     */
    @Update("${sql.updateById(et)}")
    int updateById(@Param(Param.ET) T entity);

    /**
     * 根据 主键ID 选择性的更新，只更新非null属性
     *
     * @param entity 更新实体
     * @return
     */
    @Update("${sql.updateSelectiveById(et)}")
    int updateSelectiveById(@Param(Param.ET) T entity);

    /**
     * 根据 主键ID 查询
     *
     * @param id 主键ID
     * @return
     */
    @Select("${sql.selectById(id)}")
    Optional<T> selectById(@Param(Param.ID) ID id);

    /**
     * 根据 主键ID 批量查询
     *
     * @param ids 主键ID列表
     * @return
     */
    @Select("${sql.selectBatchByIds(ids)}")
    Optional<List<T>> selectBatchByIds(@Param(Param.IDS) List<ID> ids);

}
