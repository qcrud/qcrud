package com.qcrud.exception;

import java.io.Serial;

public class UtilityClassException extends UnsupportedOperationException {

    @Serial
    private static final long serialVersionUID = 1L;

    public UtilityClassException() {
        super("utility class");
    }
}
