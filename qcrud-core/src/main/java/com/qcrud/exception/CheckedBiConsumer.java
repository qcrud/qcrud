package com.qcrud.exception;

@FunctionalInterface
public interface CheckedBiConsumer<X, Y> {
    void accept(X x, Y y) throws Throwable;
}
