package com.qcrud.exception;

@FunctionalInterface
public interface CheckedCallable<T> {
    T call() throws Throwable;
}
