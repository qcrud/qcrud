package com.qcrud.exception;

@FunctionalInterface
public interface CheckedFunction<X, T> {
    T apply(X x) throws Throwable;
}
