package com.qcrud.exception;


import com.qcrud.core.transaction.DummyException;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.lang.reflect.InvocationTargetException;

public class Sneaky {
    private Sneaky() {
        throw new UtilityClassException();
    }

    public static DummyException throwAnyway(Throwable t) {
        if (t instanceof Error error) {
            throw error;
        }

        if (t instanceof RuntimeException runtimeException) {
            throw runtimeException;
        }

        if (t instanceof IOException ioException) {
            throw new UncheckedIOException(ioException);
        }

        if (t instanceof InterruptedException) {
            Thread.currentThread().interrupt();
        }

        if (t instanceof InvocationTargetException) {
            throw throwAnyway(t.getCause());
        }

        throw throwEvadingChecks(t);
    }

    @SuppressWarnings({"unchecked", "TypeParameterUnusedInFormals"})
    private static <E extends Throwable> E throwEvadingChecks(Throwable throwable) throws E {
        throw (E) throwable;
    }
}
