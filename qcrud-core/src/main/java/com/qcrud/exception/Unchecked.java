package com.qcrud.exception;

import java.util.concurrent.Callable;
import java.util.function.*;

public class Unchecked {
    private Unchecked() {
        throw new UtilityClassException();
    }

    public static <T> Consumer<T> consumer(CheckedConsumer<T> checkedConsumer) {
        return (x) -> {
            try {
                checkedConsumer.accept(x);
            } catch (Throwable t) {
                throw Sneaky.throwAnyway(t);
            }
        };
    }

    @SuppressWarnings("PMD.DoNotUseThreads")
    public static Runnable runnable(CheckedRunnable checkedRunnable) {
        return () -> {
            try {
                checkedRunnable.run();
            } catch (Throwable t) {
                throw Sneaky.throwAnyway(t);
            }
        };
    }

    public static <T> SneakyCallable<T> callable(CheckedCallable<T> checkedCallable) {
        return () -> {
            try {
                return checkedCallable.call();
            } catch (Throwable t) {
                throw Sneaky.throwAnyway(t);
            }
        };
    }

    public static <T> Supplier<T> supplier(CheckedSupplier<T> checkedSupplier) {
        return () -> {
            try {
                return checkedSupplier.get();
            } catch (Throwable t) {
                throw Sneaky.throwAnyway(t);
            }
        };
    }

    public static <X, T> Function<X, T> function(CheckedFunction<X, T> checkedFunction) {
        return x -> {
            try {
                return checkedFunction.apply(x);
            } catch (Throwable t) {
                throw Sneaky.throwAnyway(t);
            }
        };
    }

    public static <X, Y, T> BiFunction<X, Y, T> biFunction(CheckedBiFunction<X, Y, T> checkedBiFunction) {
        return (x, y) -> {
            try {
                return checkedBiFunction.apply(x, y);
            } catch (Throwable t) {
                throw Sneaky.throwAnyway(t);
            }
        };
    }

    public static <X, Y> BiConsumer<X, Y> biConsumer(CheckedBiConsumer<X, Y> checkedBiConsumer) {
        return (x, y) -> {
            try {
                checkedBiConsumer.accept(x, y);
            } catch (Throwable t) {
                throw Sneaky.throwAnyway(t);
            }
        };
    }

    public interface SneakyCallable<T> extends Callable<T> {
        @Override
        T call(); // no 'throws Exception'
    }

    public interface CheckedRunnable {
        void run() throws Exception;
    }
}
