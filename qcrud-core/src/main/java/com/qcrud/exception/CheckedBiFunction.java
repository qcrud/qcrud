package com.qcrud.exception;

@FunctionalInterface
public interface CheckedBiFunction<X, Y, T> {
    T apply(X x, Y y) throws Throwable;
}
