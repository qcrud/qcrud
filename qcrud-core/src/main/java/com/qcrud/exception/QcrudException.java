package com.qcrud.exception;

/**
 * Qcrud 框架异常类
 *
 * @author hubin
 * @since 2022-08-18
 */
public class QcrudException extends RuntimeException {

    public QcrudException(String message) {
        super(message);
    }

    public QcrudException(Throwable throwable) {
        super(throwable);
    }

    public QcrudException(String message, Throwable cause) {
        super(message, cause);
    }

}
