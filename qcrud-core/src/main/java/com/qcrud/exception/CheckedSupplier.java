package com.qcrud.exception;

@FunctionalInterface
public interface CheckedSupplier<T> {
    T get() throws Throwable;
}
