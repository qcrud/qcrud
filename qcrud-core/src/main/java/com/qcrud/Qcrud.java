package com.qcrud;

import com.qcrud.core.*;
import com.qcrud.core.datasource.ConnectionFactory;
import com.qcrud.core.datasource.DatasourceConnectionFactory;
import com.qcrud.core.datasource.SingleConnectionFactory;
import com.qcrud.core.paging.DialectFactory;
import com.qcrud.core.paging.Paging;
import com.qcrud.core.parsing.MdSqlParser;
import com.qcrud.core.result.QueryResult;
import com.qcrud.core.result.ReturnInfo;
import com.qcrud.core.spi.QcrudPlugin;
import com.qcrud.core.transaction.TransactionIsolationLevel;
import com.qcrud.core.type.FieldTypeHandler;
import com.qcrud.core.type.TypeHandler;
import com.qcrud.exception.QcrudException;
import com.qcrud.exception.Unchecked;
import com.qcrud.jte.JteSqlTemplate;
import lombok.extern.slf4j.Slf4j;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.ServiceLoader;
import java.util.concurrent.CopyOnWriteArrayList;

@Slf4j
public class Qcrud {
    private final CopyOnWriteArrayList<QcrudPlugin> plugins = new CopyOnWriteArrayList<>();
    private final ConnectionFactory connectionFactory;
    private Configuration configuration = new Configuration();
    private SqlTemplate sqlTemplate = new JteSqlTemplate();
    private List<SqlHandler> sqlHandlerList;

    private Qcrud(ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    public static Qcrud create(DataSource dataSource) {
        return create(dataSource, null);
    }

    public static Qcrud create(DataSource dataSource, String filename) {
        return create(new DatasourceConnectionFactory(dataSource), filename);
    }

    public static Qcrud create(Connection connection) {
        return create(connection, null);
    }

    public static Qcrud create(Connection connection, String filename) {
        return create(new SingleConnectionFactory(connection), filename);
    }

    public static Qcrud create(ConnectionFactory connectionFactory, String filename) {
        Qcrud qcrud = new Qcrud(connectionFactory);
        if (null != filename) {
            try {
                // 解析 Mapper md 文件
                MdSqlParser.parse(filename);
            } catch (IOException e) {
                throw new QcrudException("Failed to parse mapper.md file");
            }
        }
        return qcrud;
    }

    public Qcrud setSqlTemplate(SqlTemplate sqlTemplate) {
        if (null != sqlTemplate) {
            this.sqlTemplate = sqlTemplate;
        }
        return this;
    }

    public Qcrud setConfiguration(Configuration configuration) {
        if (null != configuration) {
            this.configuration = configuration;
        }
        return this;
    }

    public SqlTemplate getSqlTemplate() {
        if (null == this.sqlTemplate) {
            throw new QcrudException("sqlTemplate Cannot be empty");
        }
        return this.sqlTemplate;
    }

    public Qcrud addSqlHandler(SqlHandler sqlHandler) {
        if (null == sqlHandlerList) {
            sqlHandlerList = new ArrayList<>();
        }
        sqlHandlerList.add(sqlHandler);
        return this;
    }

    /**
     * 添加返回字段类型处理器
     *
     * @param clazz       字段类型
     * @param typeHandler 类型处理器
     * @return
     */
    public Qcrud addTypeHandler(Class<?> clazz, TypeHandler typeHandler) {
        FieldTypeHandler.put(clazz, typeHandler);
        return this;
    }

    public static Qcrud create(final String url, final String username, final String password) {
        Objects.requireNonNull(url, "null url");
        Objects.requireNonNull(username, "null username");
        Objects.requireNonNull(password, "null password");
        try {
            return create(DriverManager.getConnection(url, username, password));
        } catch (SQLException e) {
            throw new QcrudException("get connection error", e);
        }
    }

    public Qcrud installPlugins() {
        ServiceLoader.load(QcrudPlugin.class).forEach(this::installPlugin);
        if (log.isDebugEnabled()) {
            log.debug("Automatically installed plugins {}", plugins);
        }
        return this;
    }

    public Qcrud installPlugin(QcrudPlugin plugin) {
        if (plugins.addIfAbsent(plugin)) {
            Unchecked.consumer(plugin::customizeQcrud).accept(this);
        }
        return this;
    }

    public Connection getConnection() {
        return connectionFactory.openConnection();
    }

    public Handle open() {
        return SqlManager.getHandleIfAbsent(this.getConnection(), connection -> {
            try {
                for (QcrudPlugin p : plugins) {
                    connection = p.customizeConnection(connection);
                }
                Handle handle = new Handle(this.configuration, connection);
                for (QcrudPlugin p : plugins) {
                    handle = p.customizeHandle(handle);
                }
                return handle;
            } catch (SQLException e) {
                throw new QcrudException(e);
            }
        });
    }

    public Object execute(SqlData sqlData) {
        try {
            // 开启处理器
            Handle handle = this.open();
            sqlData.setCtx(handle.getStatementContext());
            // SQL 模板引擎渲染
            if (null != sqlData.getParams()) {
                this.getSqlTemplate().render(sqlData);
            }
            // SQL 执行前处理
            if (null != sqlHandlerList) {
                for (SqlHandler sqlHandler : sqlHandlerList) {
                    sqlHandler.process(sqlData);
                }
            }
            SqlMeta sqlMeta = sqlData.getSqlMeta();
            SqlCommandType sqlCommandType = sqlMeta.getSqlCommandType();
            if (sqlCommandType == SqlCommandType.SELECT) {
                // 分页逻辑处理
                Integer fetchSize = null;
                Paging paging = sqlData.getPaging();
                if (null != paging) {
                    fetchSize = paging.getSize();

                    // 执行 COUNT 查询
                    if (paging.searchCount()) {
                        String countSql = "select count(*) from (" + sqlData.getSql() + ") t";
                        QueryResult queryResult = handle.createQuery(1, countSql);
                        Object result = queryResult.mapTo(sqlData, null, new ReturnInfo(9, false, Long.class));
                        paging.setTotal((Long) result);
                    }

                    // 分页 SQL 构建
                    DialectFactory.buildSql(sqlData);
                }
                // 执行查询
                QueryResult queryResult = handle.createQuery(fetchSize, sqlData);
                Object result = queryResult.mapTo(sqlData, sqlMeta.getReturnType());
                if (null == paging) {
                    // 非分页查询返回普通查询结果
                    return result;
                }
                // 设置分页查询记录列表
                paging.setRecords((List) result);
                return paging;
            }
            if (sqlCommandType == SqlCommandType.INSERT) {
                // 执行插入
                return handle.createInsert(sqlData);
            }
            if (sqlCommandType == SqlCommandType.UPDATE || sqlCommandType == SqlCommandType.DELETE) {
                // 执行更新或删除
                return handle.createUpdate(sqlData);
            }
        } catch (SQLException e) {
            throw new QcrudException(e);
        }
        return null;
    }

    public <T> T getMapper(Class<T> type) {
        return SqlManager.getMapper(type, this);
    }

    public <R, X extends Exception> R withHandle(HandleCallback<R, X> callback) throws X {
        return callback.withHandle(this.open());
    }

    public <X extends Exception> void useHandle(final HandleConsumer<X> consumer) throws X {
        withHandle(consumer.asCallback());
    }

    public <R, X extends Exception> R inTransaction(final HandleCallback<R, X> callback) throws X {
        return this.open().inTransaction(callback);
    }

    public <X extends Exception> void useTransaction(final HandleConsumer<X> consumer) throws X {
        inTransaction(consumer.asCallback());
    }

    public <R, X extends Exception> R inTransaction(TransactionIsolationLevel level, HandleCallback<R, X> callback) throws X {
        return this.open().inTransaction(level, callback);
    }

    public <X extends Exception> void useTransaction(TransactionIsolationLevel level, HandleConsumer<X> consumer) throws X {
        inTransaction(level, consumer.asCallback());
    }
}
