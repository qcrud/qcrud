package com.qcrud.jte.output;

import com.qcrud.jte.TemplateOutput;

import java.io.PrintWriter;
import java.io.Writer;

public record PrintWriterOutput(PrintWriter writer) implements TemplateOutput {

    @Override
    public Writer getWriter() {
        return writer;
    }

    @Override
    public void writeContent(String value) {
        writer.write(value);
    }
}
