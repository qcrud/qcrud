package com.qcrud.jte.output;

import com.qcrud.jte.TemplateOutput;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.io.Writer;

public record WriterOutput(Writer writer) implements TemplateOutput {

    @Override
    public Writer getWriter() {
        return writer;
    }

    @Override
    public void writeContent(String value) {
        try {
            writer.write(value);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
