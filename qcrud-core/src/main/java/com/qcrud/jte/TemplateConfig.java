package com.qcrud.jte;

import com.qcrud.jte.runtime.Constants;

import java.nio.file.Path;
import java.util.List;

public class TemplateConfig {
    public static final TemplateConfig PLAIN = new TemplateConfig(Constants.PACKAGE_NAME_PRECOMPILED);

    public final String packageName;
    public String[] compileArgs;
    public boolean trimControlStructures;
    public boolean binaryStaticContent;
    public List<String> classPath;

    public Path resourceDirectory;

    public String projectNamespace;
    public boolean generateNativeImageResources;

    public TemplateConfig(String packageName) {
        this.packageName = packageName;
    }
}
