package com.qcrud.jte.compiler;

public record TemplateDependency(String name, long lastModifiedTimestamp) {

    public String getName() {
        return name;
    }

    public long getLastModifiedTimestamp() {
        return lastModifiedTimestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TemplateDependency that = (TemplateDependency) o;

        return name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
