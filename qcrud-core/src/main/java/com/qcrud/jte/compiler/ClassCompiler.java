package com.qcrud.jte.compiler;

import com.qcrud.jte.TemplateConfig;
import com.qcrud.jte.runtime.ClassInfo;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;

public interface ClassCompiler {
    void compile(String[] files, List<String> classPath, TemplateConfig config, Path classDirectory, Map<String, ClassInfo> templateByClassName);
}
