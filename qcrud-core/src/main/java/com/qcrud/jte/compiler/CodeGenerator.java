package com.qcrud.jte.compiler;

import java.util.List;

public interface CodeGenerator extends TemplateParserVisitor {
    String getCode();

    List<byte[]> getBinaryTextParts();

    int getCurrentTemplateLine();

    static String extractTemplateExpression(String value) {
        int startIndex = value.indexOf("${");
        if (startIndex == -1) {
            return null;
        }

        int endIndex = value.lastIndexOf('}');
        if (endIndex == -1) {
            return null;
        }

        return value.substring(startIndex + 2, endIndex);
    }
}
