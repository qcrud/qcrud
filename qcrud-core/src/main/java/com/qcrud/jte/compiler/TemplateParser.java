package com.qcrud.jte.compiler;

import com.qcrud.jte.CodeResolver;
import com.qcrud.jte.TemplateConfig;
import com.qcrud.jte.resolve.DirectoryCodeResolver;
import com.qcrud.jte.runtime.StringUtils;

import java.util.*;

public final class TemplateParser {
    private final String templateCode;
    private final TemplateType type;
    private final TemplateParserVisitor visitor;
    private final TemplateConfig config;
    private final boolean trimControlStructures;

    private final Deque<Mode> stack = new ArrayDeque<>();
    private final Deque<Indent> indentStack = new ArrayDeque<>();
    private final CodeResolver codeResolver;

    private Mode currentMode;
    private Mode previousControlStructureTrimmed;
    private List<Mode> initialModes;
    private int depth;
    private boolean paramsComplete;
    private int i;
    private int startIndex;
    private int endIndex;

    private int lastIndex = 0;
    private int lastLineIndex = 0;
    private int lastTrimmedIndex = -1;

    private char previousChar;
    private char currentChar;

    public TemplateParser(String templateCode, TemplateType type, TemplateParserVisitor visitor, TemplateConfig config) {
        this(templateCode, type, visitor, config, null);
    }

    public TemplateParser(String templateCode, TemplateType type, TemplateParserVisitor visitor, TemplateConfig config, CodeResolver codeResolver) {
        this.templateCode = templateCode;
        this.type = type;
        this.visitor = visitor;
        this.config = config;
        this.trimControlStructures = config.trimControlStructures;
        this.startIndex = 0;
        this.endIndex = templateCode.length();

        this.codeResolver = codeResolver;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
        this.lastIndex = startIndex;
        this.lastLineIndex = startIndex;
    }

    public void setEndIndex(int endIndex) {
        this.endIndex = endIndex;
    }

    public void setInitialModes(List<Mode> initialModes) {
        this.initialModes = initialModes;
    }

    public void setParamsComplete(boolean paramsComplete) {
        this.paramsComplete = paramsComplete;
    }

    public void parse() {
        parse(0);
    }

    public void parse(int startingDepth) {
        doParse(startingDepth);
    }

    private void doParse(int startingDepth) {
        currentMode = Mode.Text;
        stack.push(currentMode);

        if (initialModes != null) {
            for (Mode initialMode : initialModes) {
                push(initialMode);
            }
        }

        depth = startingDepth;

        for (i = startIndex; i < endIndex; ++i) {
            previousChar = currentChar;
            currentChar = templateCode.charAt(i);

            if (!currentMode.isComment() && regionMatches("@import") && isParamOrImportAllowed()) {
                push(Mode.Import);
                lastIndex = i + 1;
            } else if (currentMode == Mode.Import && currentChar == '\n') {
                extract(templateCode, lastIndex, i, (depth, content) -> visitor.onImport(content.trim()));
                pop();
                lastIndex = i + 1;
            } else if (!currentMode.isComment() && regionMatches("@param") && isParamOrImportAllowed()) {
                push(Mode.Param);
                lastIndex = i + 1;
            } else if (currentMode == Mode.Param && currentChar == '\n') {
                extract(templateCode, lastIndex, i, (depth, content) -> visitor.onParam(content.trim()));
                pop();
                lastIndex = i + 1;
            } else if (currentMode == Mode.Text && regionMatches("@raw")) {
                extractTextPart(i - 3, Mode.Raw);
                lastIndex = i + 1;
                push(Mode.Raw);

                visitor.onRawStart(depth);
            } else if (currentMode == Mode.Content && regionMatches("@raw")) {
                push(Mode.Raw);
            } else if (currentMode == Mode.Raw && regionMatches("@endraw")) {
                pop();

                if (currentMode == Mode.Text) {
                    extractTextPart(i - 6, Mode.RawEnd);
                    lastIndex = i + 1;

                    visitor.onRawEnd(depth);
                }
            } else if (isCommentAllowed() && regionMatches("<%--")) {
                extractComment(Mode.Comment, i - 3);
            } else if (currentMode == Mode.Comment) {
                if (regionMatches("--%>")) {
                    pop();
                    lastIndex = i + 1;
                }
            } else if (currentMode == Mode.Text && regionMatches("${")) {
                extractTextPart(i - 1, Mode.Code);
                lastIndex = i + 1;
                push(Mode.Code);
            } else if (currentMode == Mode.Text && regionMatches("$unsafe{")) {
                extractTextPart(i - 7, Mode.UnsafeCode);
                lastIndex = i + 1;
                push(Mode.UnsafeCode);
            } else if (currentMode == Mode.Text && regionMatches("!{")) {
                extractTextPart(i - 1, Mode.CodeStatement);
                lastIndex = i + 1;
                push(Mode.CodeStatement);
            } else if (currentChar == '}' && currentMode == Mode.CodeStatement) {
                pop();
                if (currentMode == Mode.Text) {
                    extract(templateCode, lastIndex, i, visitor::onCodeStatement);
                    lastIndex = i + 1;
                }
            } else if (currentChar == '"' && currentMode.isTrackStrings()) {
                push(Mode.JavaCodeString);
            } else if (currentChar == '"' && currentMode == Mode.JavaCodeString && previousChar != '\\') {
                pop();
            } else if (currentChar == '}' && currentMode == Mode.Code) {
                pop();
                if (currentMode == Mode.Text) {
                    extractCodePart();
                }
            } else if (currentChar == '}' && currentMode == Mode.UnsafeCode) {
                pop();
                if (currentMode == Mode.Text) {
                    extract(templateCode, lastIndex, i, visitor::onUnsafeCodePart);
                    lastIndex = i + 1;
                }
            } else if (currentMode == Mode.Text && regionMatches("@if")) {
                extractTextPart(i - 2, Mode.Condition);
                lastIndex = i + 1;
                push(Mode.Condition);
            } else if (currentChar == '(' && (currentMode == Mode.Condition || currentMode == Mode.ConditionElse || currentMode == Mode.ForLoop)) {
                lastIndex = i + 1;
                push(new JavaCodeMode(currentChar, getCurrentTemplateLine()));
            } else if (currentMode.isTrackBraces() && (currentChar == '(' || currentChar == '{')) {
                push(new JavaCodeMode(currentChar, getCurrentTemplateLine()));
            } else if (currentMode.isTrackBraces() && (currentChar == ')' || currentChar == '}')) {
                if (currentMode == Mode.JavaCodeParam) {
                    TemplateCallMode previousMode = getPreviousMode(TemplateCallMode.class);
                    extract(templateCode, lastIndex, i, (d, c) -> {
                        if (!StringUtils.isBlank(c)) {
                            previousMode.params.add(c);
                        }
                    });
                } else if (currentMode instanceof JavaCodeMode) {
                    JavaCodeMode javaCodeMode = (JavaCodeMode) currentMode;
                    char closingBrace = javaCodeMode.getClosingBrace();
                    if (currentChar != closingBrace) {
                        visitor.onError("Unexpected closing brace " + currentChar + ", expected " + closingBrace);
                    }
                }

                pop();

                if (currentMode == Mode.Text) {
                    visitor.onError("Unexpected closing brace " + currentChar);
                } else if (currentMode == Mode.Condition) {
                    extract(templateCode, lastIndex, i, visitor::onConditionStart);
                    lastIndex = i + 1;
                    push(Mode.Text);
                } else if (currentMode == Mode.ConditionElse) {
                    extract(templateCode, lastIndex, i, visitor::onConditionElse);
                    lastIndex = i + 1;
                    push(Mode.Text);
                } else if (currentMode == Mode.ForLoop) {
                    extract(templateCode, lastIndex, i, visitor::onForLoopStart);
                    lastIndex = i + 1;
                    push(Mode.Text);
                } else if (currentMode instanceof TemplateCallMode) {
                    TemplateCallMode templateCallMode = (TemplateCallMode) currentMode;
                    extract(templateCode, lastIndex, i, (d, c) -> visitor.onTemplateCall(d, templateCallMode.name.toString(), templateCallMode.params));
                    lastIndex = i + 1;
                    pop();
                }
            } else if (regionMatches("@`") && isContentExpressionAllowed()) {
                push(Mode.Content);
            } else if (currentChar == '`' && currentMode == Mode.Content) {
                pop();
            } else if (currentChar == ',' && currentMode == Mode.JavaCodeParam) {
                TemplateCallMode previousMode = getPreviousMode(TemplateCallMode.class);
                extract(templateCode, lastIndex, i, (d, c) -> {
                    if (!StringUtils.isBlank(c)) {
                        previousMode.params.add(c);
                    }
                });
                lastIndex = i + 1;
            } else if (currentMode == Mode.Text && regionMatches("@else") && nextChar() != 'i') {
                extractTextPart(i - 4, Mode.ConditionElse);
                lastIndex = i + 1;

                pop();

                visitor.onConditionElse(depth);
                push(Mode.Text);
            } else if (currentMode == Mode.Text && regionMatches("@elseif")) {
                extractTextPart(i - 6, Mode.ConditionElse);
                lastIndex = i + 1;

                pop();

                if (currentMode == Mode.Condition || currentMode == Mode.ConditionElse) {
                    pop();
                }

                push(Mode.ConditionElse);

            } else if (currentMode == Mode.Text && regionMatches("@endif")) {
                extractTextPart(i - 5, Mode.ConditionEnd);
                lastIndex = i + 1;

                pop();

                if (currentMode == Mode.Condition || currentMode == Mode.ConditionElse) {
                    visitor.onConditionEnd(depth);
                    pop();
                }
            } else if (currentMode == Mode.Text && regionMatches("@for")) {
                extractTextPart(i - 3, Mode.ForLoop);
                lastIndex = i + 1;
                push(Mode.ForLoop);
            } else if (currentMode == Mode.Text && regionMatches("@endfor")) {
                extractTextPart(i - 6, Mode.ForLoopEnd);
                lastIndex = i + 1;

                pop();

                if (currentMode == Mode.ForLoop) {
                    visitor.onForLoopEnd(depth);
                    pop();
                }
            } else if (currentMode == Mode.Text && regionMatches("@template.")) {
                TemplateCallMode mode = new TemplateCallMode();
                extractTextPart(i - 9, mode);
                lastIndex = i + 1;

                push(mode);
                push(Mode.TemplateCallName);
            } else if (currentMode == Mode.Text && (regionMatches("@tag.") || regionMatches("@layout."))) {
                String rootDirectory = "jte-root-directory";
                if (codeResolver instanceof DirectoryCodeResolver) {
                    rootDirectory = ((DirectoryCodeResolver) codeResolver).getRoot().toAbsolutePath().toString().replace("\\", "\\\\");
                }

                visitor.onError("@tag and @layout have been replace with @template since jte 2.\nYour templates must be migrated. You can do this automatically by running the following Java code in your project:\n\n" +
                    "public class Migration {\n" +
                    "    public static void main(String[] args) {\n" +
                    "        com.qcrud.jte.migrate.MigrateV1To2.migrateTemplates(java.nio.file.Paths.get(\"" + rootDirectory + "\"));\n" +
                    "    }\n" +
                    "}");
            } else if (currentMode == Mode.TemplateCallName) {
                if (currentChar == '(') {
                    pop();
                    push(Mode.JavaCodeParam);
                    lastIndex = i + 1;
                } else if (currentChar != ' ') {
                    getPreviousMode(TemplateCallMode.class).name.append(currentChar);
                }
            }

            if (currentChar == '\n' && currentMode != Mode.Content) {
                visitor.onLineFinished();
                lastLineIndex = i + 1;
            }
        }

        if (stack.size() > 1) {
            handleUnclosedKeywords();
        }

        if (lastIndex < endIndex) {
            extractTextPart(endIndex, null);
        }

        if (type != TemplateType.Content) {
            completeParamsIfRequired();
            visitor.onComplete();
        }
    }

    private char nextChar() {
        if (i + 1 >= templateCode.length()) {
            return 0;
        }

        return templateCode.charAt(i + 1);
    }

    private boolean regionMatches(String s) {
        return templateCode.regionMatches(i - s.length() + 1, s, 0, s.length());
    }

    private void handleUnclosedKeywords() {
        while (currentMode == Mode.Text) {
            pop();
        }

        if (currentMode instanceof JavaCodeMode) {
            JavaCodeMode mode = (JavaCodeMode) currentMode;
            visitor.onError("Missing closing brace " + mode.getClosingBrace(), mode.getTemplateLine());
        } else if (currentMode == Mode.Condition || currentMode == Mode.ConditionElse) {
            visitor.onError("Missing @endif");
        } else if (currentMode == Mode.ForLoop) {
            visitor.onError("Missing @endfor");
        }
    }

    private int getCurrentTemplateLine() {
        if (visitor instanceof CodeGenerator) {
            return ((CodeGenerator) visitor).getCurrentTemplateLine();
        }

        return 0;
    }

    private boolean isCommentAllowed() {
        return currentMode == Mode.Text;
    }

    private boolean isParamOrImportAllowed() {
        if (paramsComplete) {
            return false;
        }

        int endIndex = templateCode.lastIndexOf('@', this.i);
        for (int j = lastIndex; j < endIndex; j++) {
            char currentChar = templateCode.charAt(j);
            if (!Character.isWhitespace(currentChar)) {
                return false;
            }
        }
        return true;
    }

    private boolean areParamsComplete(int startIndex) {
        if (visitor instanceof TemplateParametersCompleteVisitor) {
            return false;
        }

        try {
            TemplateParser templateParser = new TemplateParser(templateCode, type, new TemplateParametersCompleteVisitor(), config);
            templateParser.setStartIndex(startIndex);
            templateParser.parse();
        } catch (TemplateParametersCompleteVisitor.Result result) {
            if (result.complete) {
                return true;
            }
        }

        return false;
    }

    private boolean isContentExpressionAllowed() {
        return currentMode == Mode.Content || currentMode == Mode.JavaCodeParam || currentMode == Mode.Code || currentMode == Mode.CodeStatement || currentMode == Mode.Param;
    }

    private void extractTextPart(int endIndex, Mode mode) {
        if (currentMode != Mode.Text) {
            visitor.onError("Unexpected end of template expression");
        }

        if (trimControlStructures) {
            extractTextPartAndTrimControlStructures(endIndex, mode);
        } else {
            extract(templateCode, lastIndex, endIndex, visitor::onTextPart);
        }
    }

    private void extractTextPartAndTrimControlStructures(int endIndex, Mode mode) {
        completeParamsIfRequired();

        int startIndex = lastIndex;
        if (lastTrimmedIndex != -1) {
            if (lastTrimmedIndex > lastIndex) {
                startIndex = lastTrimmedIndex;
            }
            lastTrimmedIndex = -1;
        }

        if (startIndex < 0) {
            return;
        }
        if (endIndex < startIndex) {
            return;
        }

        if (LineInfo.isSingleControlStructure(templateCode, endIndex, this.endIndex, lastLineIndex, mode)) {
            lastTrimmedIndex = templateCode.indexOf('\n', endIndex) + 1;
            extractTrimmed(startIndex, lastLineIndex);
            previousControlStructureTrimmed = mode;
        } else {
            extractTrimmed(startIndex, endIndex);
            previousControlStructureTrimmed = null;
        }

        if (mode == Mode.Condition || mode == Mode.ForLoop || mode == Mode.Raw) {
            pushIndent(endIndex, mode);
        } else if (mode == Mode.ConditionEnd || mode == Mode.ForLoopEnd || mode == Mode.RawEnd) {
            popIndent();
        }
    }

    private void extractTrimmed(int startIndex, int endIndex) {
        int indentationsToSkip = getIndentationsToSkip();
        visitor.onTextPart(depth, trimIndentations(startIndex, endIndex, indentationsToSkip));
    }

    private String trimIndentations(int startIndex, int endIndex, int indentationsToSkip) {
        StringBuilder resultText = new StringBuilder(endIndex - startIndex);

        int indentation = 0;
        int line = 0;
        boolean writeLine = false;
        boolean firstNonWhitespaceReached = false;
        if (previousControlStructureTrimmed == null) {
            firstNonWhitespaceReached = true;
            writeLine = true;
        }

        for (int j = startIndex; j < endIndex; ++j) {
            char currentChar = templateCode.charAt(j);

            if ((line > 0 || firstNonWhitespaceReached) && (currentChar == '\r' || currentChar == '\n')) {
                resultText.append(currentChar);
            }

            if (currentChar == '\r') {
                continue;
            }

            if (currentChar == '\n') {
                ++line;
                indentation = 0;
                writeLine = false;
                continue;
            }

            if (!firstNonWhitespaceReached && !Character.isWhitespace(currentChar)) {
                firstNonWhitespaceReached = true;
            }

            if (!writeLine && isIndentationCharacter(currentChar) && indentation < indentationsToSkip) {
                ++indentation;
            } else {
                writeLine = true;
            }

            if (writeLine) {
                resultText.append(currentChar);
            }
        }

        return resultText.toString();
    }

    private void pushIndent(int endIndex, Mode mode) {
        int currentLineIndentation = 0;
        for (int j = endIndex - 1; j >= lastIndex; --j) {
            char currentChar = templateCode.charAt(j);
            if (isIndentationCharacter(currentChar)) {
                ++currentLineIndentation;
            } else {
                break;
            }
        }

        int nextLineIndentation = 0;
        int nextIndex = templateCode.indexOf('\n', endIndex);
        if (nextIndex > 0) {
            for (int j = nextIndex + 1; j < this.endIndex; ++j) {
                char currentChar = templateCode.charAt(j);
                if (isIndentationCharacter(currentChar)) {
                    ++nextLineIndentation;
                } else {
                    break;
                }
            }
        }

        int amount = Math.max(nextLineIndentation - currentLineIndentation, 0);

        indentStack.push(new Indent(mode, amount));
    }

    private void popIndent() {
        if (!indentStack.isEmpty()) {
            indentStack.pop();
        }
    }

    private boolean isIndentationCharacter(char c) {
        return c == ' ' || c == '\t';
    }

    private int getIndentationsToSkip() {
        int amount = 0;
        for (Indent indent : indentStack) {
            amount += indent.amount;
        }
        return amount;
    }

    private void extractCodePart() {
        extractPlainCodePart();
        lastIndex = i + 1;
    }

    private void extractPlainCodePart() {
        extract(templateCode, lastIndex, i, visitor::onCodePart);
    }

    private void extractComment(Mode mode, int startIndex) {
        if (paramsComplete || areParamsComplete(startIndex)) {
            extractTextPart(startIndex, mode);
        }
        push(mode);
    }

    private int getLastWhitespaceIndex(int index) {
        for (; index >= 0; --index) {
            if (!Character.isWhitespace(templateCode.charAt(index))) {
                ++index;
                break;
            }
        }
        return index;
    }

    private void push(Mode mode) {
        currentMode = mode;
        stack.push(currentMode);
        if (mode == Mode.Text) {
            ++depth;
        }
    }

    private void pop() {
        Mode previousMode = stack.pop();
        if (previousMode == Mode.Text) {
            --depth;
        }

        currentMode = stack.peek();
    }

    @SuppressWarnings({"SameParameterValue", "unchecked"})
    private <T extends Mode> T getPreviousMode(Class<T> modeClass) {
        for (Mode mode : stack) {
            if (modeClass.isAssignableFrom(mode.getClass())) {
                return (T) mode;
            }
        }
        throw new IllegalStateException("Expected mode of type " + modeClass + " on the stack, but found nothing!");
    }

    private void extract(String templateCode, int startIndex, int endIndex, VisitorCallback callback) {
        completeParamsIfRequired();

        if (startIndex < 0) {
            return;
        }
        if (endIndex < startIndex) {
            return;
        }
        callback.accept(depth, templateCode.substring(startIndex, endIndex));
    }

    private void completeParamsIfRequired() {
        if (!paramsComplete && currentMode != Mode.Param && currentMode != Mode.Import && type != TemplateType.Content) {
            visitor.onParamsComplete();
            paramsComplete = true;
        }
    }

    interface VisitorCallback {
        void accept(int depth, String content);
    }

    interface Mode {
        Mode Import = new StatelessMode("Import");
        Mode Param = new StatelessMode("Param");
        Mode Text = new StatelessMode("Text");
        Mode Code = new StatelessMode("Code", true, true, false);
        Mode UnsafeCode = new StatelessMode("UnsafeCode", true, true, false);
        Mode CodeStatement = new StatelessMode("CodeStatement", true, true, false);
        Mode Condition = new StatelessMode("Condition");
        Mode JavaCodeParam = new StatelessMode("JavaCodeParam", true, true, false);
        Mode JavaCodeString = new StatelessMode("JavaCodeString");
        Mode ConditionElse = new StatelessMode("ConditionElse");
        Mode ConditionEnd = new StatelessMode("ConditionEnd");
        Mode ForLoop = new StatelessMode("ForLoop");
        Mode ForLoopEnd = new StatelessMode("ForLoopEnd");
        Mode TemplateCallName = new StatelessMode("TemplateCallName");
        Mode Comment = new StatelessMode("Comment");
        Mode Content = new StatelessMode("Content", false, false, true);
        Mode Raw = new StatelessMode("Raw");
        Mode RawEnd = new StatelessMode("RawEnd");

        boolean isTrackStrings();

        boolean isTrackBraces();

        boolean isComment();
    }

    private static class StatelessMode implements Mode {
        private final String debugName;
        private final boolean trackStrings;
        private final boolean trackBraces;
        private final boolean comment;

        private StatelessMode(String debugName) {
            this(debugName, false, false, false);
        }

        private StatelessMode(String debugName, boolean trackStrings, boolean trackBraces, boolean comment) {
            this.debugName = debugName;
            this.trackStrings = trackStrings;
            this.trackBraces = trackBraces;
            this.comment = comment;
        }

        @Override
        public boolean isTrackStrings() {
            return trackStrings;
        }

        @Override
        public boolean isTrackBraces() {
            return trackBraces;
        }

        @Override
        public boolean isComment() {
            return comment;
        }

        @Override
        public String toString() {
            return getClass().getSimpleName() + "[" + debugName + "]";
        }
    }

    private static class TemplateCallMode implements Mode {
        final StringBuilder name = new StringBuilder();
        final List<String> params = new ArrayList<>();

        @Override
        public boolean isTrackStrings() {
            return false;
        }

        @Override
        public boolean isTrackBraces() {
            return false;
        }

        @Override
        public boolean isComment() {
            return false;
        }
    }

    private static class JavaCodeMode implements Mode {

        private final char closingBrace;
        private final int templateLine;

        public JavaCodeMode(char openingBrace, int templateLine) {
            this.closingBrace = openingBrace == '(' ? ')' : '}';
            this.templateLine = templateLine;
        }

        @Override
        public boolean isTrackStrings() {
            return true;
        }

        @Override
        public boolean isTrackBraces() {
            return true;
        }

        @Override
        public boolean isComment() {
            return false;
        }

        public char getClosingBrace() {
            return closingBrace;
        }

        public int getTemplateLine() {
            return templateLine;
        }
    }

    private static class Indent {
        public final Mode mode;
        public final int amount;

        public Indent(Mode mode, int amount) {
            this.mode = mode;
            this.amount = amount;
        }
    }
}
