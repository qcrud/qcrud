package com.qcrud.jte.runtime;

public enum TemplateMode {
    OnDemand,
    Precompiled,
}
