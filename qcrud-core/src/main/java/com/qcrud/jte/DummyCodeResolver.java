package com.qcrud.jte;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;

public class DummyCodeResolver implements CodeResolver {
    private static final Map<String, String> codeLookup = new ConcurrentHashMap<>();

    @Override
    public String resolve(String name) {
        return codeLookup.get(name);
    }

    @Override
    public long getLastModified(String name) {
        return 0;
    }

    public void givenCode(String name, Supplier<String> code) {
        if (null == codeLookup.get(name)) {
            // 同一个模板文件值初始化一次
            codeLookup.put(name, code.get());
        }
    }

    @Override
    public List<String> resolveAllTemplateNames() {
        return new ArrayList<>(codeLookup.keySet());
    }
}
