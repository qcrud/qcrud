package com.qcrud.core.parsing;

import com.qcrud.annotation.IdType;
import lombok.Getter;
import lombok.Setter;

import java.lang.reflect.Field;

@Getter
@Setter
public class IdInfo extends ColumnInfo {
    /**
     * ID 类型默认自增
     */
    private IdType idType;

    public IdInfo(String camelToUnderline, String fieldName, boolean convert, Field field) {
        super(camelToUnderline, fieldName, convert, field);
    }

    /**
     * ID 数据库自增
     *
     * @return true 是 false 否
     */
    public boolean autoincrement() {
        return IdType.AUTO == idType;
    }
}
