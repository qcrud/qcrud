package com.qcrud.core.parsing;

import com.esotericsoftware.reflectasm.MethodAccess;
import com.qcrud.core.SqlData;
import com.qcrud.core.SqlManager;
import com.qcrud.core.SqlParameter;
import com.qcrud.core.type.FieldTypeHandler;
import com.qcrud.core.type.TypeHandler;
import lombok.Getter;
import lombok.Setter;

import java.lang.reflect.Field;
import java.util.Locale;
import java.util.function.Function;

@Getter
@Setter
public class ColumnInfo {
    /**
     * 字段名
     */
    private String name;
    /**
     * 属性名
     */
    private String property;
    /**
     * 属性 Get 方法名
     */
    private String methodName;
    /**
     * 是否类型转换
     */
    private boolean convert;
    /**
     * 类型处理器
     */
    private TypeHandler typeHandler;

    public ColumnInfo(String name, String property, boolean convert, Field field) {
        this.name = name;
        this.property = property;
        this.methodName = property.substring(0, 1).toUpperCase(Locale.ROOT) + property.substring(1);
        this.convert = convert;
        this.typeHandler = FieldTypeHandler.get(field.getType());
    }

    public void bindSqlParameter(SqlData sqlData, int index, Object obj) {
        this.bindSqlParameter(sqlData, index, obj, null);
    }

    public void bindSqlParameter(SqlData sqlData, int index, Object obj, Function<Object, Boolean> func) {
        MethodAccess methodAccess = SqlManager.getMethodAccess(sqlData.getEntityClass());
        Object value = methodAccess.invoke(obj, "get" + methodName);
        if (null == func || func.apply(value)) {
            sqlData.addPosition(index, new SqlParameter(value, typeHandler));
        }
    }
}
