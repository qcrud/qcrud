package com.qcrud.core.transaction;


import java.io.Serial;

public class DummyException extends RuntimeException {

    @Serial
    private static final long serialVersionUID = 1L;

    private DummyException() {
        throw new UnsupportedOperationException("this exception should never actually be instantiated or thrown");
    }
}
