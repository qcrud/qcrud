package com.qcrud.core.transaction;

import com.qcrud.exception.QcrudException;

public class TransactionException extends QcrudException {

    public TransactionException(String string, Throwable throwable) {
        super(string, throwable);
    }

    public TransactionException(Throwable throwable) {
        super(throwable);
    }

    public TransactionException(String msg) {
        super(msg);
    }
}