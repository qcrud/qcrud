package com.qcrud.core.transaction;

import com.qcrud.core.Handle;
import com.qcrud.core.HandleCallback;

import java.sql.SQLException;

public interface TransactionHandler {

    void begin(Handle handle);

    void commit(Handle handle);

    void rollback(Handle handle);

    boolean isInTransaction(Handle handle);

    void savepoint(Handle handle, String savepointName);

    void rollbackToSavepoint(Handle handle, String savepointName);

    void releaseSavepoint(Handle handle, String savepointName);

    <R, X extends Exception> R inTransaction(Handle handle,
                                             HandleCallback<R, X> callback) throws X;

    <R, X extends Exception> R inTransaction(Handle handle,
                                             TransactionIsolationLevel level,
                                             HandleCallback<R, X> callback) throws X;

    default TransactionHandler specialize(final Handle handle) throws SQLException {
        return this;
    }
}
