package com.qcrud.core.transaction;

import com.qcrud.exception.QcrudException;

public class UnableToRestoreAutoCommitStateException  extends QcrudException {

    public UnableToRestoreAutoCommitStateException(Throwable throwable) {
        super(throwable);
    }
}
