package com.qcrud.core;

import com.qcrud.core.paging.Paging;
import com.qcrud.exception.QcrudException;
import com.qcrud.tookit.ConstantPool;
import lombok.Getter;
import lombok.Setter;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Getter
@Setter
public class SqlData {
    private Binding binding;
    private String statementKey;
    private StatementContext ctx;
    private Class entityClass;
    private SqlMeta sqlMeta;
    private String sql;
    private Map<String, Object> params;

    public SqlData(SqlMeta sqlMeta, String statementKey, Class entityClass) {
        this.binding = new Binding();
        this.sqlMeta = sqlMeta;
        this.statementKey = statementKey;
        this.entityClass = entityClass;
    }

    public SqlData addPosition(int position, SqlParameter parameter) {
        this.getBinding().addPosition(position, parameter);
        return this;
    }

    public SqlData addPosition(List<Object> arguments) {
        Binding binding = this.getBinding();
        int i = 1;
        for (Object arg : arguments) {
            binding.addPosition(i++, SqlParameter.of(arg));
        }
        return this;
    }

    public PreparedStatement getPreparedStatement() {
        return this.ctx.getStmt();
    }

    public String getSql() {
        return null == sql ? sqlMeta.getRawSql() : sql;
    }

    public Paging getPaging() {
        return null == params ? null : (Paging) params.get(ConstantPool.PAGING_KEY);
    }

    /**
     * 绑定 SQL 执行参数
     *
     * @param ps {@link PreparedStatement}
     */
    public void binding(PreparedStatement ps) {
        if (!binding.isEmpty()) {
            binding.forEach((index, sqlParameter) -> {
                if (null == sqlParameter) {
                    try {
                        ps.setObject(index, null);
                    } catch (SQLException e) {
                        throw new QcrudException(e);
                    }
                }
                sqlParameter.setParameter(ps, index);
            });
        }
    }
}
