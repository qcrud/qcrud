package com.qcrud.core.spi;

import com.qcrud.Qcrud;
import com.qcrud.core.Handle;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Qcrud 插件类 spi 实现
 *
 */
public interface QcrudPlugin {

    default void customizeQcrud(Qcrud qcrud) throws SQLException {}

    default Handle customizeHandle(Handle handle) throws SQLException {
        return handle;
    }

    default Connection customizeConnection(Connection conn) throws SQLException {
        return conn;
    }

}
