package com.qcrud.core;

import java.util.HashMap;

public class Binding extends HashMap<Integer, SqlParameter> {

    public void addPosition(int position, SqlParameter parameter) {
        this.put(position, parameter);
    }
}
