package com.qcrud.core;

@FunctionalInterface
public interface HandleConsumer<X extends Exception> {

    void useHandle(Handle handle) throws X;

    default HandleCallback<Void, X> asCallback() {
        return h -> {
            useHandle(h);
            return null;
        };
    }
}
