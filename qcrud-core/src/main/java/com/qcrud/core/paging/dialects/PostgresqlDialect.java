package com.qcrud.core.paging.dialects;

/**
 * Postgresql 数据库分页语句组装实现
 */
public class PostgresqlDialect implements IDialect {

    @Override
    public String buildSql(String originalSql, long offset, long limit) {
        StringBuilder sql = new StringBuilder(originalSql);
        sql.append(" LIMIT ").append(limit).append(" OFFSET ").append(offset);
        return sql.toString();
    }
}
