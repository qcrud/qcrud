package com.qcrud.core.paging.dialects;

public interface IDialect {

    /**
     * 组装分页语句
     *
     * @param originalSql 原始语句
     * @param offset      偏移量
     * @param limit       界限
     * @return 分页模型
     */
    String buildSql(String originalSql, long offset, long limit);
}
