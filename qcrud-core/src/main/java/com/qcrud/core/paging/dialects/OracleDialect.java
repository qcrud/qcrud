package com.qcrud.core.paging.dialects;

public class OracleDialect implements IDialect {

    @Override
    public String buildSql(String originalSql, long offset, long limit) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT * FROM ( SELECT TMP.*, ROWNUM ROW_ID FROM ( ").append(originalSql);
        sql.append(" ) TMP WHERE ROWNUM <=").append((offset >= 1) ? (offset + limit) : limit);
        sql.append(") WHERE ROW_ID > ").append(offset);
        return sql.toString();
    }
}
