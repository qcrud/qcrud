package com.qcrud.core.paging.dialects;

/**
 * Mysql 数据库分页语句组装实现
 */
public class MySqlDialect implements IDialect {

    @Override
    public String buildSql(String originalSql, long offset, long limit) {
        StringBuilder sql = new StringBuilder(originalSql);
        sql.append(" LIMIT ").append(offset).append(",").append(limit);
        return sql.toString();
    }
}
