package com.qcrud.core.sql;

import java.util.List;

/**
 * CRUD SQL 接口
 *
 */
public interface CrudSql {

    /**
     * 插入 SQL
     *
     * @param entity 插入实体
     * @return
     */
    String insert(Object entity);

    /**
     * 批量插入 SQL
     *
     * @param entityList 插入实体列表
     * @return
     */
    String insertBatch(List<Object> entityList);

    /**
     * 根据 主键ID 删除 SQL
     *
     * @param id 主键ID
     * @return
     */
    String deleteById(Object id);

    /**
     * 根据 主键ID 批量删除 SQL
     *
     * @param ids 主键ID列表
     * @return
     */
    String deleteBatchByIds(List<Object> ids);

    /**
     * 根据 主键ID 更新 SQL
     *
     * @param entity 更新实体
     * @return
     */
    String updateById(Object entity);

    /**
     * 根据 主键ID 选择性的更新 SQL
     *
     * @param entity 更新实体
     * @return
     */
    String updateSelectiveById(Object entity);

    /**
     * 根据 主键ID 查询 SQL
     *
     * @param id 主键ID
     * @return
     */
    String selectById(Object id);

    /**
     * 根据 主键ID 批量查询 SQL
     *
     * @param ids 主键ID列表
     * @return
     */
    String selectBatchByIds(List<Object> ids);
}
