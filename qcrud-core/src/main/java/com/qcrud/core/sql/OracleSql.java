package com.qcrud.core.sql;

import com.qcrud.core.SqlData;

/**
 * Oracle CRUD SQL 实现
 *
 */
public class OracleSql extends StandardSql {

    public OracleSql(SqlData sqlData) {
        super(sqlData);
    }
}
