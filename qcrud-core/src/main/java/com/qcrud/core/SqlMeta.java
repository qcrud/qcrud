package com.qcrud.core;

import lombok.Getter;
import lombok.Setter;

import java.lang.reflect.Type;

@Getter
@Setter
public class SqlMeta {
    private SqlCommandType sqlCommandType;
    private String rawSql;
    private Type returnType;

    public SqlMeta(SqlCommandType sqlCommandType, String rawSql) {
        this(sqlCommandType, rawSql, null);
    }

    public SqlMeta(SqlCommandType sqlCommandType, String rawSql, Type returnType) {
        this.sqlCommandType = sqlCommandType;
        this.rawSql = rawSql;
        this.returnType = returnType;
    }

}
