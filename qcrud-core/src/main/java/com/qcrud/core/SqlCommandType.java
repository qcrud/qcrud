package com.qcrud.core;

public enum SqlCommandType {
    // 插入
    INSERT,
    // 删除
    DELETE,
    // 更新
    UPDATE,
    // 查询
    SELECT,
    // 存储过程
    CALL;

    public static SqlCommandType of(String methodName) {
        SqlCommandType[] sqlCommandTypes = SqlCommandType.values();
        for (SqlCommandType sct : sqlCommandTypes) {
            if (methodName.toUpperCase().startsWith(sct.name())) {
                return sct;
            }
        }
        // 其它命令默认为查询
        return SELECT;
    }
}
