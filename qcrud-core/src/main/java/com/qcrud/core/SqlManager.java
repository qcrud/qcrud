package com.qcrud.core;

import com.esotericsoftware.reflectasm.MethodAccess;
import com.qcrud.Qcrud;
import com.qcrud.annotation.IdType;
import com.qcrud.annotation.Param;
import com.qcrud.core.datasource.DbType;
import com.qcrud.core.key.AutoincrementKey;
import com.qcrud.core.key.KeyGenerator;
import com.qcrud.core.key.KeyInfo;
import com.qcrud.core.parsing.IdInfo;
import com.qcrud.core.parsing.TableInfo;
import com.qcrud.core.sql.CrudSql;
import com.qcrud.core.sql.OracleSql;
import com.qcrud.core.sql.StandardSql;
import com.qcrud.mapper.MapperProxyFactory;
import com.qcrud.tookit.ClassUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

public class SqlManager {
    private static final Map<Class, Object> MAPPER_MAP = new ConcurrentHashMap<>();
    private static final Map<Class, Class> ENTITY_CLASS_MAP = new ConcurrentHashMap<>();
    private static final Map<Class, MethodAccess> METHOD_ACCESS_MAP = new ConcurrentHashMap<>();
    private static final Map<String, SqlMeta> SQL_META_MAP = new ConcurrentHashMap<>();
    private static final Map<Class, TableInfo> TABLE_INFO_MAP = new ConcurrentHashMap<>();
    private static final Map<String, String> SQL_MAP = new ConcurrentHashMap<>();
    private static final Map<Connection, Handle> HANDLE_MAP = new ConcurrentHashMap<>();
    private static final Map<String, List<Param>> PARAMS_MAP = new ConcurrentHashMap<>();

    /**
     * 主键生成器
     */
    private static final Map<String, KeyInfo> KEY_INFO_MAP = new ConcurrentHashMap<>();
    private static final Map<IdType, KeyGenerator> KEY_MAP = new ConcurrentHashMap<>() {{
        put(IdType.AUTO, new AutoincrementKey());
    }};

    public static KeyInfo getKeyInfo(SqlData sqlData) {
        return KEY_INFO_MAP.computeIfAbsent(sqlData.getStatementKey(), key -> {
            KeyInfo keyInfo = null;
            Map<String, Object> params = sqlData.getParams();
            // 单行插入
            Object obj = params.get(Param.ET);
            if (null != obj) {
                keyInfo = getKeyInfo(obj.getClass());
            } else {
                // 批量插入
                List<Object> objs = (List<Object>) params.get(Param.ETS);
                if (null != objs) {
                    keyInfo = getKeyInfo(objs.get(0).getClass());
                }
            }
            return keyInfo;
        });
    }

    public static KeyInfo getKeyInfo(Class<?> clazz) {
        TableInfo tableInfo = SqlManager.getTableInfo(clazz);
        if (null == tableInfo) {
            return null;
        }
        IdInfo idInfo = tableInfo.getIdInfo();
        if (null == idInfo) {
            return null;
        }
        KeyGenerator keyGenerator = getKeyGenerator(idInfo.getIdType());
        if (null == keyGenerator) {
            return null;
        }
        return new KeyInfo(keyGenerator, idInfo);
    }

    public static KeyGenerator getKeyGenerator(IdType idType) {
        return null == idType ? null : KEY_MAP.get(idType);
    }

    /**
     * 获取 Mapper 代理对象
     *
     * @param cls   Mapper Class
     * @param qcrud {@link Qcrud}
     * @return
     */
    public static <T> T getMapper(Class<T> cls, Qcrud qcrud) {
        return (T) MAPPER_MAP.computeIfAbsent(cls, t -> {
            Method[] methods = cls.getMethods();
            for (Method method : methods) {
                // Mapper 泛型类
                Class entityClass = getGenericEntityClass(cls);
                // 泛型实体映射表信息解析
                getTableInfo(entityClass);
                // 反射方法访问类
                getMethodAccess(entityClass);
                // 方法参数
                getMethodParams(getStatementKey(method), method);
            }
            // 返回代理 Mapper
            return new MapperProxyFactory<T>(cls).newInstance(qcrud);
        });
    }

    public static SqlMeta put(String key, SqlMeta sqlMeta) {
        return SQL_META_MAP.put(key, sqlMeta);
    }

    /**
     * 获取 Mapper 泛型实体 Class
     *
     * @param mapper Mapper 接口类
     * @return
     */
    public static Class getGenericEntityClass(Class mapper) {
        return ENTITY_CLASS_MAP.computeIfAbsent(mapper, k -> {
            ParameterizedType parameterizedType = (ParameterizedType) mapper.getGenericInterfaces()[0];
            Type beanClazz = parameterizedType.getActualTypeArguments()[0];
            return ClassUtils.getClass(beanClazz);
        });
    }

    /**
     * asm reflect MethodAccess
     *
     * @param entityClass 泛型实体 Class
     * @return
     */
    public static MethodAccess getMethodAccess(Class entityClass) {
        return METHOD_ACCESS_MAP.computeIfAbsent(entityClass, k -> MethodAccess.get(entityClass));
    }

    public static SqlMeta get(String key) {
        return SQL_META_MAP.get(key);
    }

    /**
     * 泛型实体映射表信息解析
     *
     * @param clazz 表映射实体
     * @return
     */
    public static TableInfo getTableInfo(Class clazz) {
        return TABLE_INFO_MAP.computeIfAbsent(clazz, t -> TableInfo.parse(clazz));
    }

    public static String getSqlIfAbsent(String statementKey, Function<String, String> mappingFunction) {
        return SQL_MAP.computeIfAbsent(statementKey, mappingFunction);
    }

    public static Handle getHandleIfAbsent(Connection connection, Function<Connection, Handle> handleFunction) {
        return HANDLE_MAP.computeIfAbsent(connection, handleFunction);
    }

    public static List<Param> getMethodParams(String statementKey, Method method) {
        return PARAMS_MAP.computeIfAbsent(statementKey, t -> {
            List<Param> sqlParams = new ArrayList<>();
            Annotation[][] anArr = method.getParameterAnnotations();
            for (Annotation[] ans : anArr) {
                for (Annotation an : ans) {
                    if (an instanceof Param) {
                        sqlParams.add((Param) an);
                    }
                }
            }
            return sqlParams;
        });
    }

    /**
     * 返回一个不可修改的 MdSqlMap
     */
    public static Map<String, SqlMeta> getSqlMetaMap() {
        return Collections.unmodifiableMap(SQL_META_MAP);
    }

    /**
     * Mapper Method StatementKey
     *
     * @param method {@link Method}
     * @return
     */
    public static String getStatementKey(Method method) {
        String declaringClassName = method.getDeclaringClass().getName();
        return declaringClassName + "." + method.getName();
    }

    /**
     * 初始化 crud sql 生成实现类
     *
     * @param sqlData {@link SqlData}
     * @return {@link CrudSql}
     */
    public static CrudSql initCrudSql(SqlData sqlData) {
        StatementContext ctx = sqlData.getCtx();
        DbType dbType = ctx.getDbType();

        if (dbType == DbType.ORACLE || dbType == DbType.ORACLE_12C) {
            return new OracleSql(sqlData);
        }

        // 标准 SQL 生成
        return new StandardSql(sqlData);
    }
}
