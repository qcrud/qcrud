package com.qcrud.core.result;

/**
 * 调用 Mapper 方法返回结果信息
 * <p>
 * type 结果类型 0，列表 1，Map集合 2，单个实体 其它基本类型
 * </p>
 */
public record ReturnInfo(Integer type, Boolean optional, Class<?> beanClass) {

    public boolean isOptional() {
        return null == optional ? false : optional;
    }
}
