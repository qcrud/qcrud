package com.qcrud.core.datasource;

import java.sql.Connection;
import java.sql.SQLException;

@FunctionalInterface
public interface ConnectionFactory {

    Connection openConnection();

    default void closeConnection(Connection conn) throws SQLException {
        conn.close();
    }
}
