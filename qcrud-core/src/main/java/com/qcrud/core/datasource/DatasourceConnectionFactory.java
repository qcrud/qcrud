package com.qcrud.core.datasource;

import com.qcrud.exception.QcrudException;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 */
public record DatasourceConnectionFactory(DataSource dataSource) implements ConnectionFactory {
    static ThreadLocal<Connection> connectionThreadLocal = new ThreadLocal<>();

    @Override
    public Connection openConnection() {
        Connection conn = connectionThreadLocal.get();
        if (null == conn) {
            try {
                conn = dataSource.getConnection();
                connectionThreadLocal.set(conn);
            } catch (SQLException e) {
                throw new QcrudException("get connection error.", e);
            }
        }
        return conn;
    }
}
