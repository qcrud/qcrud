package com.qcrud.core.datasource;

import java.sql.Connection;

/**
 *
 */
public record SingleConnectionFactory(Connection connection) implements ConnectionFactory {

    @Override
    public Connection openConnection() {
        return connection;
    }
}
