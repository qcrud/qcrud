package com.qcrud.core;

public interface SqlHandler {

    void process(SqlData sqlData);
}
