package com.qcrud.core;

import com.qcrud.core.type.FieldTypeHandler;
import com.qcrud.core.type.TypeHandler;
import com.qcrud.exception.QcrudException;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public record SqlParameter(Object parameter, TypeHandler typeHandler) {

    public static SqlParameter of(Object value) {
        return null == value ? null : new SqlParameter(value, FieldTypeHandler.get(value.getClass()));
    }

    public void setParameter(PreparedStatement ps, int index) {
        try {
            typeHandler.setParameter(ps, index, parameter);
        } catch (SQLException e) {
            throw new QcrudException("binding args error");
        }
    }
}
