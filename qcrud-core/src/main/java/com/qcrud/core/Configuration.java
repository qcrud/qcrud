package com.qcrud.core;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Configuration {
    /**
     * Jdbc Statement fetchSize
     */
    private int fetchSize = 100;

}
