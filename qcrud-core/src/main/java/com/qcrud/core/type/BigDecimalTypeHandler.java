package com.qcrud.core.type;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BigDecimalTypeHandler implements TypeHandler<BigDecimal> {

    @Override
    public void setParameter(PreparedStatement ps, int index, BigDecimal parameter) throws SQLException {
        ps.setBigDecimal(index, parameter);
    }

    @Override
    public BigDecimal getResult(ResultSet rs, int columnIndex) throws SQLException {
        return rs.getBigDecimal(columnIndex);
    }
}
