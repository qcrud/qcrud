package com.qcrud.core.type;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Month;

public class MonthTypeHandler implements TypeHandler<Month> {

    @Override
    public void setParameter(PreparedStatement ps, int index, Month parameter) throws SQLException {
        ps.setInt(index, parameter.getValue());
    }

    @Override
    public Month getResult(ResultSet rs, int columnIndex) throws SQLException {
        int month = rs.getInt(columnIndex);
        return month == 0 && rs.wasNull() ? null : Month.of(month);
    }
}
