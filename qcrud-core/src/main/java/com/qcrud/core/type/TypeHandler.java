package com.qcrud.core.type;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public interface TypeHandler<T> {

    /**
     * 设置参数
     * @param ps 预编译的Statement
     * @param index 参数索引
     * @param parameter 参数值
     * @throws SQLException
     */
    void setParameter(PreparedStatement ps, int index, T parameter) throws SQLException;

    /**
     * 从结果集获取结果
     * @param rs 结果集
     * @param columnIndex 结果集中的列的索引位置
     * @return
     * @throws SQLException
     */
    T getResult(ResultSet rs, int columnIndex) throws SQLException;
}
