package com.qcrud.core.type;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum DataType {
    String(String.class),
    Integer(Integer.class),
    Long(Long.class),
    Boolean(Boolean.class),
    Double(Double.class),
    Float(Float.class),
    Short(Short.class),
    Byte(Byte.class),
    Character(Character.class);

    private Class clazz;

    public static DataType of(Class cls) {
        DataType[] bts = DataType.values();
        for (DataType bt : bts) {
            if (bt.getClazz().equals(cls)) {
                return bt;
            }
        }
        return null;
    }
}
