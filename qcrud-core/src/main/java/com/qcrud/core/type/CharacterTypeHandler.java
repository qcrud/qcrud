package com.qcrud.core.type;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CharacterTypeHandler implements TypeHandler<Character> {

    @Override
    public void setParameter(PreparedStatement ps, int index, Character parameter) throws SQLException {
        ps.setString(index, parameter.toString());
    }

    @Override
    public Character getResult(ResultSet rs, int columnIndex) throws SQLException {
        String columnValue = rs.getString(columnIndex);
        if (columnValue != null && !columnValue.isEmpty()) {
            return columnValue.charAt(0);
        }
        return null;
    }
}
