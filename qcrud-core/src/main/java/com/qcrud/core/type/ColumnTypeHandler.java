package com.qcrud.core.type;

import lombok.Getter;
import lombok.Setter;

import java.sql.ResultSet;
import java.sql.SQLException;

@Getter
@Setter
public class ColumnTypeHandler<T> {
    private String column;
    private TypeHandler typeHandler;

    public ColumnTypeHandler(String column, TypeHandler typeHandler) {
        this.column = column;
        this.typeHandler = typeHandler;
    }

    public <T> T getResult(ResultSet rs, int columnIndex) throws SQLException {
        return (T) typeHandler.getResult(rs, columnIndex);
    }
}
