package com.qcrud.core.type;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;

public class InstantTypeHandler implements TypeHandler<Instant> {

    @Override
    public void setParameter(PreparedStatement ps, int index, Instant parameter) throws SQLException {
        ps.setTimestamp(index, Timestamp.from(parameter));
    }

    @Override
    public Instant getResult(ResultSet rs, int columnIndex) throws SQLException {
        Timestamp timestamp = rs.getTimestamp(columnIndex);
        return getInstant(timestamp);
    }

    private static Instant getInstant(Timestamp timestamp) {
        return null == timestamp ? null : timestamp.toInstant();
    }
}
