package com.qcrud.core.type;

import com.qcrud.tookit.ClassUtils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Clob;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class FieldTypeHandler {
    private static final UnknownTypeHandler unknownTypeHandler = new UnknownTypeHandler();
    private static final Map<String, TypeHandler> classNameTypeHandlerCache = new HashMap<>();
    private static final Map<Class, TypeHandler> classTypeHandlerCache = new HashMap<>() {{
        put(String.class, new StringTypeHandler());
        put(Integer.class, new IntegerTypeHandler());
        put(Long.class, new LongTypeHandler());
        put(Boolean.class, new BooleanTypeHandler());
        put(BigDecimal.class, new BigDecimalTypeHandler());
        put(Double.class, new DoubleTypeHandler());
        put(Float.class, new FloatTypeHandler());
        put(Short.class, new ShortTypeHandler());
        put(BigInteger.class, new BigIntegerTypeHandler());
        put(Date.class, new DateTypeHandler());
        put(Timestamp.class, new LocalDateTimeTypeHandler());
        put(LocalDate.class, new LocalDateTypeHandler());
        put(LocalTime.class, new LocalTimeTypeHandler());
        put(Month.class, new MonthTypeHandler());
        put(Clob.class, new ClobTypeHandler());
        put(Character.class, new CharacterTypeHandler());
        put(Instant.class, new InstantTypeHandler());
    }};

    public static void put(Class<?> clazz, TypeHandler typeHandler) {
        classTypeHandlerCache.put(clazz, typeHandler);
    }

    public static TypeHandler get(String className) {
        return classNameTypeHandlerCache.computeIfAbsent(className, t -> get(ClassUtils.forName(className)));
    }

    public static TypeHandler get(Class<?> clazz) {
        TypeHandler typeHandler = null;
        if (null != clazz) {
            typeHandler = classTypeHandlerCache.get(clazz);
        }
        return null == typeHandler ? unknownTypeHandler : typeHandler;
    }
}
