package com.qcrud.core.type;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BigIntegerTypeHandler implements TypeHandler<BigInteger> {

    @Override
    public void setParameter(PreparedStatement ps, int index, BigInteger parameter) throws SQLException {
        ps.setBigDecimal(index, new BigDecimal(parameter));
    }

    @Override
    public BigInteger getResult(ResultSet rs, int columnIndex) throws SQLException {
        BigDecimal bigDecimal = rs.getBigDecimal(columnIndex);
        return bigDecimal == null ? null : bigDecimal.toBigInteger();
    }
}
