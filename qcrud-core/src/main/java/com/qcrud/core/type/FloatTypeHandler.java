package com.qcrud.core.type;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class FloatTypeHandler implements TypeHandler<Float> {

    @Override
    public void setParameter(PreparedStatement ps, int index, Float parameter) throws SQLException {
        ps.setFloat(index, parameter);
    }

    @Override
    public Float getResult(ResultSet rs, int columnIndex) throws SQLException {
        float result = rs.getFloat(columnIndex);
        return result == 0 && rs.wasNull() ? null : result;
    }
}
