package com.qcrud.core.type;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ShortTypeHandler implements TypeHandler<Short> {

    @Override
    public void setParameter(PreparedStatement ps, int index, Short parameter) throws SQLException {
        ps.setShort(index, parameter);
    }

    @Override
    public Short getResult(ResultSet rs, int columnIndex) throws SQLException {
        short result = rs.getShort(columnIndex);
        return result == 0 && rs.wasNull() ? null : result;
    }
}
