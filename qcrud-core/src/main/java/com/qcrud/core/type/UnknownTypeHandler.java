package com.qcrud.core.type;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UnknownTypeHandler implements TypeHandler<Object> {

    @Override
    public void setParameter(PreparedStatement ps, int index, Object parameter) throws SQLException {
        ps.setObject(index, parameter);
    }

    @Override
    public Object getResult(ResultSet rs, int columnIndex) throws SQLException {
        return rs.getObject(columnIndex);
    }
}
