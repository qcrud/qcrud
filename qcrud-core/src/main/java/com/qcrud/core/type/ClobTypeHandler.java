package com.qcrud.core.type;

import java.io.StringReader;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ClobTypeHandler implements TypeHandler<String> {

    @Override
    public void setParameter(PreparedStatement ps, int index, String parameter) throws SQLException {
        StringReader reader = new StringReader(parameter);
        ps.setCharacterStream(index, reader, parameter.length());
    }

    @Override
    public String getResult(ResultSet rs, int columnIndex) throws SQLException {
        Clob clob = rs.getClob(columnIndex);
        return toString(clob);
    }

    private String toString(Clob clob) throws SQLException {
        return clob == null ? null : clob.getSubString(1, (int) clob.length());
    }
}
