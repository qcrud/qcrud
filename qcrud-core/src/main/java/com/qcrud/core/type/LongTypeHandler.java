package com.qcrud.core.type;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LongTypeHandler implements TypeHandler<Long> {

    @Override
    public void setParameter(PreparedStatement ps, int index, Long parameter) throws SQLException {
        ps.setLong(index, parameter);
    }

    @Override
    public Long getResult(ResultSet rs, int columnIndex) throws SQLException {
        long result = rs.getLong(columnIndex);
        return result == 0 && rs.wasNull() ? null : result;
    }
}
