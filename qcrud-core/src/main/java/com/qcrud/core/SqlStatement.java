package com.qcrud.core;

import com.qcrud.core.type.ColumnTypeHandler;
import com.qcrud.core.type.FieldTypeHandler;
import com.qcrud.core.type.TypeHandler;
import com.qcrud.tookit.StringUtils;
import lombok.extern.slf4j.Slf4j;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public record SqlStatement(StatementContext ctx, String sql, PreparedStatement ps) implements AutoCloseable {

    /**
     * 绑定 SQL 执行参数
     *
     * @param args 执行SQL参数
     * @return
     */
    protected SqlStatement bindArgs(Object... args) {
        if (null != args && args.length > 0) {
            int position = 1;
            for (Object arg : args) {
                SqlParameter.of(arg).setParameter(ps, position++);
            }
        }
        return this;
    }

    public boolean execute(Object... args) throws SQLException {
        /**
         * 绑定 SQL 执行参数
         */
        this.bindArgs(args);
        /**
         * 如果第一个结果是 ResultSet 对象，则返回 true
         * 如果第一个结果是更新计数或者没有结果，则返回 false
         */
        boolean result = ps.execute();
        if (log.isDebugEnabled()) {
            log.debug("execute sql: {}; result: {}", sql.trim(), result);
        }
        return result;
    }

    public List<Map<String, Object>> executeQuery(SqlData sqlData) throws SQLException {
        // 绑定 SQL 执行参数
        sqlData.binding(ps);
        // 执行查询操作
        return this.executeQuery();
    }

    public List<Map<String, Object>> executeQuery(Object... args) throws SQLException {
        // 绑定 SQL 执行参数
        this.bindArgs(args);
        // 执行查询操作
        return this.executeQuery();
    }

    protected List<Map<String, Object>> executeQuery() throws SQLException {
        List<Map<String, Object>> dataMap = new ArrayList<>();
        Map<String, ColumnTypeHandler> ctm = new HashMap<>();
        ResultSet rs = ps.executeQuery();
        if (null != rs) {
            ResultSetMetaData rsm = rs.getMetaData();
            int rows = rsm.getColumnCount();
            while (rs.next()) {
                Map<String, Object> rowData = new HashMap<>();
                for (int j = 1; j <= rows; j++) {
                    String key = rsm.getColumnName(j);
                    ColumnTypeHandler ct = ctm.get(key);
                    if (null == ct) {
                        String newKey = StringUtils.isHump(key) ? key : StringUtils.underlineToHump(key);
                        TypeHandler typeHandler = FieldTypeHandler.get(rsm.getColumnClassName(j));
                        ct = new ColumnTypeHandler(newKey, typeHandler);
                        ctm.put(key, ct);
                    }
                    rowData.put(ct.getColumn(), ct.getResult(rs, j));
                }
                dataMap.add(rowData);
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("execute sql: {}; result: {}", sql.trim(), dataMap);
        }
        return dataMap;
    }

    public int executeUpdate(SqlData sqlData) throws SQLException {
        // 绑定 SQL 执行参数
        sqlData.binding(ps);
        // 执行插入更新操作
        return ps.executeUpdate();
    }

    public int executeUpdate(Object... args) throws SQLException {
        // 绑定 SQL 执行参数
        this.bindArgs(args);
        // 执行插入更新操作
        return ps.executeUpdate();
    }

    @Override
    public void close() throws Exception {

    }
}
