package com.qcrud.core.key;

import com.qcrud.annotation.IdType;
import com.qcrud.core.parsing.IdInfo;

public record KeyInfo(KeyGenerator keyGenerator, IdInfo idInfo) {

    public boolean autoincrement() {
        return null != idInfo && idInfo.getIdType() == IdType.AUTO;
    }
}
