package com.qcrud.core.key;

import com.qcrud.core.parsing.IdInfo;

import java.sql.Statement;

public interface KeyGenerator {

    void processBefore(Statement stmt, IdInfo idInfo, Object parameter);

    void processAfter(Statement stmt, IdInfo idInfo, Object parameter);
}
