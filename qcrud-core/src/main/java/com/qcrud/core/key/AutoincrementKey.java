package com.qcrud.core.key;

import com.esotericsoftware.reflectasm.MethodAccess;
import com.qcrud.core.SqlManager;
import com.qcrud.core.parsing.IdInfo;
import com.qcrud.exception.QcrudException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * 数据库自增 ID
 */
public class AutoincrementKey implements KeyGenerator {

    @Override
    public void processBefore(Statement stmt, IdInfo idInfo, Object parameter) {

    }

    @Override
    public void processAfter(Statement stmt, IdInfo idInfo, Object parameter) {
        try {
            ResultSet rs = stmt.getGeneratedKeys();
            if (rs.next()) {
                MethodAccess access = SqlManager.getMethodAccess(parameter.getClass());
                access.invoke(parameter, "set" + idInfo.getMethodName(), idInfo.getTypeHandler().getResult(rs, 1));
            }
        } catch (SQLException e) {
            throw new QcrudException("Error getting generated key or setting result to parameter object. Cause: " + e, e);
        }
    }
}
