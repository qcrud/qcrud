package com.qcrud.core;

public interface SqlTemplate {

    void render(SqlData sqlData);

}
