package com.qcrud.tookit;

public interface ConstantPool {
    String QCRUD = "_QCRUD_";
    String PAGING_KEY = QCRUD + "PAGE_";
    String EMPTY = "";
    char UNDERLINE = '_';
    String UNDERSCORE = "_";
}
