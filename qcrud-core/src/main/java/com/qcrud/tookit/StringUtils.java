package com.qcrud.tookit;

/**
 * 字符串工具类
 *
 * @author hubin
 * @author hcl
 */
public class StringUtils {

    /**
     * 字符串下划线转驼峰格式
     *
     * @param name 需要转换的字符串
     * @return 转换好的字符串
     */
    public static String underlineToHump(String name) {
        return NamingConventions.snakeToCamel(name, false);
    }


    /**
     * 字符串驼峰转下划线格式
     *
     * @param param 需要转换的字符串
     * @return 转换好的字符串
     */
    public static String camelToUnderline(String param) {
        return NamingConventions.camelToSnake(param);
    }

    /**
     * 判断字符串是不是驼峰命名
     *
     * <li> 包含 '_' 不算 </li>
     * <li> 首字母大写的不算 </li>
     *
     * @param str 字符串
     * @return 结果
     */
    public static boolean isHump(String str) {
        return Character.isLowerCase(str.charAt(0)) && !str.contains(ConstantPool.UNDERSCORE);
    }

}
