package com.qcrud.tookit;

import com.qcrud.exception.QcrudException;
import lombok.experimental.UtilityClass;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * This utility class main function is convert The Bean to The Map or The Map to The Bean
 *
 * @author hubin dy
 */
@SuppressWarnings("rawtypes")
@UtilityClass
public final class BeanMapUtils {

    /**
     * The class field buffer
     */
    private static final Map<Class, List<Field>> CLASS_FIELD_MAP = new ConcurrentHashMap<>();

    /**
     * Get class fields
     *
     * @param clazz {@link Class}
     * @return fields
     */
    public static List<Field> getFields(Class clazz) {
        List<Field> fields = CLASS_FIELD_MAP.get(clazz);
        if (null == fields) {
            fields = Arrays.stream(clazz.getDeclaredFields())
                .filter(t -> {
                    int mod = t.getModifiers();
                    return !(Modifier.isStatic(mod) || Modifier.isFinal(mod));
                })
                .peek(t -> t.setAccessible(true)).collect(Collectors.toList());
            CLASS_FIELD_MAP.put(clazz, fields);
        }
        return fields;
    }

    /**
     * Convert The Bean to The Map
     *
     * @param object The object to be converted
     * @return converted result
     */
    public static Map<String, Object> beanToMap(Object object) {
        try {
            List<Field> fields = getFields(object.getClass());
            if (fields == null || fields.size() == 0) {
                return Collections.emptyMap();
            }
            Map<String, Object> map = new HashMap<>(fields.size());
            for (Field field : fields) {
                map.put(field.getName(), field.get(object));
            }
            return map;
        } catch (Exception e) {
            throw new QcrudException(e);
        }
    }

    /**
     * Convert The Map to The Bean
     *
     * @param map   The map to be converted
     * @param clazz The type to be converted
     * @param <T>   generic type
     * @return converted result
     */
    public static <T> T mapToBean(Map map, Class<T> clazz) {
        try {
            T object = ClassUtils.newInstance(clazz);
            List<Field> fields = getFields(clazz);
            for (Field field : fields) {
                Object obj = map.get(field.getName());
                if (null != obj) {
                    field.set(object, obj);
                }
            }
            return object;
        } catch (Exception e) {
            throw new QcrudException(e);
        }
    }

    /**
     * Convert The Map List to The Bean List
     *
     * @param maps  The map list to be converted
     * @param clazz The type to be converted
     * @return converted result
     */
    public static <T> List<T> mapsToBeans(List<Map<String, Object>> maps, Class<T> clazz) {
        if (null == maps || maps.isEmpty()) {
            return Collections.emptyList();
        }
        return maps.stream()
            .map(e -> mapToBean(e, clazz))
            .toList();
    }
}
