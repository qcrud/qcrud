package com.qcrud.tookit;

import com.esotericsoftware.reflectasm.ConstructorAccess;
import com.qcrud.exception.QcrudException;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Objects;

public final class ClassUtils {

    /**
     * 根据指定的 class 实例化一个对象
     *
     * @param clazz 需要实例化的对象
     * @param <T>   类型，由输入类型决定
     * @return 返回新的实例
     */
    public static <T> T newInstance(Class<T> clazz) {
        return ConstructorAccess.get(clazz).newInstance();
    }

    /**
     * 返回类型泛型的类型
     *
     * @param type {@link Type}
     * @return {@link Type[]}
     */
    public static Type[] getActualTypeArguments(Type type) {
        return ParameterizedType.class.cast(type).getActualTypeArguments();
    }

    /**
     * Type 类型转为 Class
     *
     * @param type {@link Type}
     * @return {@link Class}
     */
    public static Class getClass(Type type) {
        try {
            return Class.forName(type.getTypeName());
        } catch (ClassNotFoundException e) {
            throw new QcrudException(e);
        }
    }

    /**
     * 字符串类名转换为 Class
     *
     * @param className 字符串类名
     * @return
     */
    public static Class forName(String className) {
        try {
            return Class.forName(className);
        } catch (ClassNotFoundException e) {
            throw new QcrudException(e);
        }
    }

    public static String getPackageName(Class<?> clazz) {
        Objects.requireNonNull(clazz, "Class must not be null");
        return getPackageName(clazz.getName());
    }

    public static String getPackageName(String fqClassName) {
        Objects.requireNonNull(fqClassName, "Class name must not be null");
        int lastDotIndex = fqClassName.lastIndexOf(46);
        return lastDotIndex != -1 ? fqClassName.substring(0, lastDotIndex) : "";
    }
}
