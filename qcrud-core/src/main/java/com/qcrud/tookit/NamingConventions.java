package com.qcrud.tookit;

import com.qcrud.exception.QcrudException;
import org.jetbrains.annotations.NotNull;

/**
 * 命名规范辅助工具类，在字符串上赋予字符串特定的格式含义，这些格式主要来自约定。
 * <p>
 * Create by hcl at 2022/12/10
 */
public class NamingConventions {

    /**
     * 蛇形格式命名转为驼峰格式。
     * 蛇形格式又叫下划线格式，它被定为：以小写字母开头，以小写字母或者数字结束，中间只能包含下划线、小写字母或数字，下划线后面只能跟小写字母。
     * <p>
     * 合法的格式：a_b、user_name、school
     * <p>
     * 不合法的格式：_a、internal_
     * <p>
     * 如果输入的字符串中不含有下划线或大写字母，该方法会返回原字符串。
     *
     * @param snake 蛇形格式名称
     * @param upper 是否转为大驼峰
     * @return 返回转换后的字符串
     * @see #camelToSnake(String)
     */
    @NotNull
    public static String snakeToCamel(@NotNull String snake, boolean upper) {
        char[] chars = snake.toCharArray();
        StringBuilder builder = new StringBuilder(chars.length);
        boolean converted = false;

        char c = chars[0];
        if (Character.isLowerCase(c)) {
            if (upper) {
                converted = true;
                builder.append(Character.toUpperCase(c));
            } else {
                builder.append(c);
            }
        } else {
            throw new QcrudException("Snake naming can only begin with a lowercase letter");
        }

        for (int i = 1; i < chars.length; i++) {
            c = chars[i];

            if (Character.isUpperCase(c)) {
                throw new QcrudException("Snake naming cannot have capital letters");
            }

            if (c == '_') {
                converted = true;
                char primary = chars[++i];
                if (Character.isLowerCase(primary)) {
                    builder.append(Character.toUpperCase(primary));
                } else {
                    throw new QcrudException("Snake naming can only be followed by lowercase letters");
                }
            } else if (Character.isJavaIdentifierPart(c)) {
                builder.append(c);
            } else {
                throw new QcrudException("The name cannot contain this character: " + c);
            }
        }

        return converted ? builder.toString() : snake;
    }

    /**
     * 驼峰格式命名转蛇形格式命名，该方法会自动识别大小驼峰格式。
     *
     * @return 转换后的字符串
     * @see #snakeToCamel(String, boolean)
     */
    @NotNull
    public static String camelToSnake(@NotNull String camel) {
        char[] chars = camel.toCharArray();
        StringBuilder builder = new StringBuilder(chars.length);
        boolean converted = false;

        char c = chars[0];
        if (Character.isLetter(c)) {
            if (Character.isUpperCase(c)) {
                builder.append(Character.toLowerCase(c));
                converted = true;
            } else {
                builder.append(c);
            }
        } else {
            throw new QcrudException("Camel-Case name cannot begin with: " + c);
        }

        for (int i = 1; i < chars.length; i++) {
            c = chars[i];
            if (Character.isUpperCase(c)) {
                converted = true;
                builder.append('_').append(Character.toLowerCase(c));
            } else if (Character.isJavaIdentifierPart(c)) {
                builder.append(c);
            } else {
                throw new QcrudException("The name cannot contain this character: " + c);
            }
        }

        return converted ? builder.toString() : camel;
    }

}
