package com.qcrud.records;

import com.qcrud.annotation.Column;
import com.qcrud.annotation.Id;
import com.qcrud.annotation.IdType;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class Person {
    @Id(type = IdType.AUTO)
    private Integer id;
    private String name;
    private String email;
    private LocalDateTime lastLogin;

    @Column(ignore = true)
    private String notColumn;

    public Person() {

    }

    public Person(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Person(String name, String email, LocalDateTime lastLogin) {
        this(null, name, email, lastLogin);
    }

    public Person(Integer id, String name, String email, LocalDateTime lastLogin) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.lastLogin = lastLogin;
    }

}
