package com.qcrud.records;

import com.qcrud.JteTest;
import com.qcrud.annotation.Delete;
import com.qcrud.annotation.Param;
import com.qcrud.annotation.Select;
import com.qcrud.core.paging.Paging;
import com.qcrud.mapper.QcrudMapper;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface PersonMapper extends QcrudMapper<Person, Integer> {

    @Select("""
        select name from person where id=${id}
        """)
    String selectNameById(@Param("id") int id);

    Optional<List<Person>> selectByEmailAndName(@Param("email") String email, @Param("name") String name);

    Paging<Person> pageByName(Paging<Person> page, @Param("name") String name);

    @Select("select * from person")
    List<Person> selectListAll();

    @Select("select * from person")
    List<Map<String, Object>> selectListAllResultMap();

    List<Person> selectByNameAndIds(@Param("tableName") String tableName,
                                    @Param("jt") JteTest jt,
                                    @Param("name") String name,
                                    @Param("ids") List<Integer> ids);

    @Delete("delete from person where name='${name}'")
    int deleteByName(@Param("name") String name);

    // 无法执行方法
    Person cannotExecuteMethod();
}
