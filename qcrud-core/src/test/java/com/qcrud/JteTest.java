package com.qcrud;

import com.qcrud.jte.DummyCodeResolver;
import com.qcrud.jte.TemplateEngine;
import com.qcrud.jte.output.StringOutput;
import com.qcrud.jte.resolve.ResourceCodeResolver;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class JteTest {
    StringOutput output = new StringOutput();
    DummyCodeResolver dummyCodeResolver = new DummyCodeResolver();
    TemplateEngine templateEngine = TemplateEngine.create(dummyCodeResolver);

    @Test
    public void test() {
        var codeResolver = new ResourceCodeResolver("jte");
        var templateEngine = TemplateEngine.create(codeResolver);
        var output = new StringOutput();
        Map<String, Object> params = new HashMap<>();
        params.put("name", "World");
        params.put("testIf", false);
        templateEngine.render("hello.jte", params, output);
        System.out.println(output);
    }

    public String table(String name) {
        return name;
    }

    @Test
    void test2() {
        final String templateName = "hi.jte";
        dummyCodeResolver.givenCode(templateName, () -> """
                    @import com.qcrud.JteTest
                    @param JteTest jt
                    @param String tableName
                    SELECT * FROM ${jt.table(tableName)}""");

        templateEngine.render(templateName, new HashMap<>() {{
            put("jt", new JteTest());
            put("tableName", "person");
        }}, output);
        assertEquals(output.toString(), "SELECT * FROM person");
    }
}
