package com.qcrud;

import com.qcrud.core.Handle;
import com.qcrud.core.paging.Paging;
import com.qcrud.core.result.QueryResult;
import com.qcrud.exception.QcrudException;
import com.qcrud.records.Person;
import com.qcrud.records.PersonMapper;
import org.junit.jupiter.api.*;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class MySqlTest extends DbTest {
    private String name = "qcrud";
    private String email = "abc@qcrud.com";
    private static Qcrud qcrud;

    @BeforeAll
    private static void initDb() {
        qcrud = Qcrud.create(getMySqlConnection(), "/sql/PersonMapper.md");
    }

    private PersonMapper getPersonMapper() {
        return qcrud.getMapper(PersonMapper.class);
    }

    private PersonMapper personMapper = getPersonMapper();

    @Test
    @Order(1)
    public void testMapperProxy() {
        // 插入
        assertEquals(personMapper.insert(new Person(10, name, email, LocalDateTime.now())), 1);

        // 批量插入
        LocalDateTime localDateTime = LocalDateTime.now();
        assertEquals(personMapper.insertBatch(Arrays.asList(
            new Person(8, name, email, localDateTime),
            new Person(9, name, email, localDateTime),
            new Person(11, name, email, localDateTime),
            new Person(12, name + 1, 1 + email, localDateTime),
            new Person(13, name + 2, 2 + email, localDateTime)
        )), 5);

        // 根据ID更新
        assertEquals(personMapper.updateById(new Person(10, "updateNameById")), 1);

        // 根据ID选择性的更新
        assertEquals(personMapper.updateSelectiveById(new Person(11, "updateSelectiveById")), 1);

        // 根据ID删除
        // assertEquals(personMapper.deleteById(40), 1);

        // 根据ID集合批量删除
        // assertEquals(personMapper.deleteBatchByIds(Arrays.asList(41, 42, 45)), 3);

        // 根据ID查询
        assertEquals(personMapper.selectById(12).get().getName(), name + 1);

        // 根据ID集合批量查询
        Optional<List<Person>> optionalPersonList = personMapper.selectBatchByIds(Arrays.asList(11, 12, 13));
        assertTrue(optionalPersonList.isPresent());
        assertEquals(optionalPersonList.get().size(), 3);

        // 注解查询
        String _name = personMapper.selectNameById(13);
        assertEquals(_name, name + 2);

        // sql md 查询返回 optional 结果集
        // optionalPersonList = personMapper.selectByEmailAndName(email, name);
        // assertTrue(optionalPersonList.isPresent());

        // 不存在 SQL 异常
        assertThrows(QcrudException.class, () -> personMapper.cannotExecuteMethod());
    }

    @Test
    @Order(2)
    public void testPaging() {
        assertTrue(personMapper.selectListAll().size() > 1);
        assertTrue(personMapper.selectListAllResultMap().size() > 1);

        List<Person> personList = personMapper.selectByNameAndIds("person",
            new JteTest(), name, Arrays.asList(11, 12));
        assertTrue(personList.size() > 0);

        Paging<Person> personPaging = personMapper.pageByName(Paging.of(1, 10), name);
        assertTrue(personPaging.getTotal() > 0);
        // 测试项目模板的执行 JTE 是否重新加载并生成 jte-classes 文件
        Paging<Person> personPaging2 = personMapper.pageByName(Paging.of(1, 2), name);
        assertTrue(personPaging2.getTotal() > 0);
    }


    @Test
    @Order(3)
    public void testAutoKey() {
        // 插入ID自增
        Person person = new Person(name, email, LocalDateTime.now());
        assertNull(person.getId());
        assertEquals(personMapper.insert(person), 1);
        assertNotNull(person.getId());
    }

    @Test
    @Order(5)
    void testOpen() throws SQLException {
        Handle handle = qcrud.open();
        boolean result = handle.execute("""
                insert person (name,email,last_login) values (?,?,?)
            """, "testOpen", "testOpen@qcrud.com", LocalDateTime.now());
        assertFalse(result);

        List<Map<String, Object>> data = handle.createQuery("select * from person").dataList();
        assertTrue(data.size() > 0);
    }

    @Test
    @Order(6)
    void testSqlHandle() throws SQLException {
        qcrud.useHandle(handle -> {
            assertEquals(1, handle.createUpdate("update person set email=? where id=?", email, 1));

            QueryResult queryResult = handle.createQuery("select * from person where id=?", 1);
            Person person = (Person) queryResult.mapTo(Person.class);
            if (null != person) {
                assertEquals(person.getEmail(), email);
            }
        });
    }

    @Test
    @Order(7)
    void testSqlHandleTransaction() {
        assertThrows(QcrudException.class, () -> qcrud.useTransaction(handle -> {
            QueryResult queryResult = handle.createQuery("select * from person where id=?", 1);
            Person person = (Person) queryResult.mapTo(Person.class);
            if (null != person) {
                assertEquals(person.getEmail(), email);
            }

            System.out.println(handle.execute("""
                    insert person (name,email,last_login) values (?,?,?)
                """, "123abc", "123abc@qcrud.com", LocalDateTime.now()));

            throw new QcrudException("error rollback");
        }));
    }

    /**
     * 自定义删除测试方法
     */
    @Test
    @Order(8)
    void testDetete() {
        Person person = new Person("sdk", "sdk@qcrud.com", LocalDateTime.now());
        assertEquals(personMapper.insert(person), 1);
        assertEquals(personMapper.deleteByName("sdk"), 1);
    }
}
