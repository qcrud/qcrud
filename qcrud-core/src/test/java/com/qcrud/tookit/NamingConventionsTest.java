package com.qcrud.tookit;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Create by hcl at 2022/12/10
 */
class NamingConventionsTest {

    @Test
    void snakeToCamel() {
        assertEquals("user", NamingConventions.snakeToCamel("user", false));
        assertEquals("userName", NamingConventions.snakeToCamel("user_name", false));
        assertThrows(Exception.class, () -> NamingConventions.snakeToCamel("", false));
    }

    @Test
    void camelToSnake() {
        assertEquals("user", NamingConventions.camelToSnake("user"));
        assertEquals("user", NamingConventions.camelToSnake("User"));
    }

}