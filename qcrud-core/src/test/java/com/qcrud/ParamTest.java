package com.qcrud;

import com.qcrud.annotation.Param;
import com.qcrud.core.SqlManager;
import com.qcrud.records.PersonMapper;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class ParamTest {

    @Test
    public void sqlParam() throws Exception {
        Method method = PersonMapper.class.getMethod("selectByEmailAndName", String.class, String.class);
        List<Param> sqlParams = SqlManager.getMethodParams(method.getName(), method);
        assertTrue("email".equals(sqlParams.get(0).value()));
        assertTrue("name".equals(sqlParams.get(1).value()));
    }
}
