package com.qcrud;

import com.qcrud.core.SqlCommandType;
import com.qcrud.core.SqlMeta;
import com.qcrud.core.parsing.MdSqlParser;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class MdSqlParserTest {

    @Test
    public void parseDir() throws Exception {
        Map<String, SqlMeta> mdSqlMap = MdSqlParser.parse("/sql");
        assertFalse(mdSqlMap.isEmpty());
    }

    @Test
    public void parse() throws Exception {
        Map<String, SqlMeta> mdSqlMap = MdSqlParser.parse("/sql/mapper.md");
        SqlMeta select = mdSqlMap.get("com.qcrud.records.PersonMapper.selectNameById");
        assertEquals(select.getSqlCommandType(), SqlCommandType.SELECT);
        assertEquals(select.getRawSql(), "select name from person where id=?");

        SqlMeta insert = mdSqlMap.get("com.qcrud.records.PersonMapper.insert");
        assertEquals(insert.getSqlCommandType(), SqlCommandType.INSERT);
        assertEquals(insert.getRawSql(), "insert person (name,email,last_login) values (?,?,?)");

        SqlMeta delete = mdSqlMap.get("com.qcrud.records.PersonMapper.deleteById");
        assertEquals(delete.getSqlCommandType(), SqlCommandType.DELETE);
        assertEquals(delete.getRawSql(), "delete from person where id=?");

        SqlMeta update = mdSqlMap.get("com.qcrud.records.PersonMapper.updateEmailById");
        assertEquals(update.getSqlCommandType(), SqlCommandType.UPDATE);
        assertEquals(update.getRawSql(), "update person set email=? where id=?");

        assertEquals(mdSqlMap.get("com.qcrud.records.UserMapper.selectById").getRawSql(), "select * from user where id=?");
    }
}
