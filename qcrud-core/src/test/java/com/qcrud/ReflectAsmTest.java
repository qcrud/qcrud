package com.qcrud;

import com.esotericsoftware.reflectasm.MethodAccess;
import com.qcrud.records.Person;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ReflectAsmTest {

    @Test
    public void test() {
        String name = "qcrud";
        String email = "abc@qcrud.com";
        Person person = new Person();
        person.setEmail(email);
        MethodAccess access = MethodAccess.get(Person.class);
        access.invoke(person, "setName", name);
        assertEquals(name, access.invoke(person, "getName"));
        assertEquals(email, access.invoke(person, "getEmail"));
    }
}
