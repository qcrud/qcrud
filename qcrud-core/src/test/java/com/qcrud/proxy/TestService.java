package com.qcrud.proxy;

public interface TestService {

    String test(String s);
}
