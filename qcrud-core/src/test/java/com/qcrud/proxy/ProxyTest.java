package com.qcrud.proxy;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

@State(Scope.Benchmark)
public class ProxyTest {
    @Param({"10", "20", "50"})
    private int dataCount;

    @Benchmark
    public void jdk() {
        InvokeHandler handler = new InvokeHandler();
        TestService testService = ProxyEnum.JDK_PROXY.newProxyInstance(TestService.class, handler);
        testService.test("jdk");
    }

    @Benchmark
    public void byteBuddy() {
        InvokeHandler handler = new InvokeHandler();
        TestService testService = ProxyEnum.BYTE_BUDDY_PROXY.newProxyInstance(TestService.class, handler);
        testService.test("byteBuddy");
    }

    /*
        -------------------------- 测试结果 JDK 动态代理更加优秀 --------------------------
        Benchmark            (dataCount)   Mode  Cnt         Score         Error  Units
        TestProxy.byteBuddy            3  thrpt   25      1746.400 ±     144.559  ops/s
        TestProxy.jdk                  3  thrpt   25  49917467.143 ± 6570707.796  ops/s
        -------------------------------------------------------------------------------
    */
    public static void main(String[] args) throws RunnerException {
        Options options = new OptionsBuilder()
            .include(ProxyTest.class.getSimpleName())
            .shouldDoGC(true)
            .resultFormat(ResultFormatType.JSON)
            .result("benchmark-report.json")
            .shouldFailOnError(true)
            .jvmArgs("-server")
            .build();
        new Runner(options).run();
    }
}
