package com.qcrud.proxy;

import com.qcrud.tookit.ClassUtils;
import net.bytebuddy.ByteBuddy;
import net.bytebuddy.dynamic.loading.ClassLoadingStrategy;
import net.bytebuddy.implementation.MethodDelegation;
import net.bytebuddy.matcher.ElementMatchers;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

public enum ProxyEnum {
    JDK_PROXY(new ProxyFactory() {
        public <T> T newProxyInstance(Class<T> interfaceClass, Object handler) {
            return interfaceClass.cast(Proxy.newProxyInstance(this.getClass().getClassLoader(),
                new Class[]{interfaceClass}, (InvocationHandler) handler));
        }
    }),
    BYTE_BUDDY_PROXY(new ProxyFactory() {
        public <T> T newProxyInstance(Class<T> interfaceClass, Object handler) {
            Class<? extends T> cls = new ByteBuddy()
                .subclass(interfaceClass)
                .method(ElementMatchers.isDeclaredBy(interfaceClass))
                .intercept(MethodDelegation.to(handler, "handler"))
                .make()
                .load(interfaceClass.getClassLoader(), ClassLoadingStrategy.Default.INJECTION)
                .getLoaded();
            return ClassUtils.newInstance(cls);
        }
    });

    private ProxyFactory factory;

    ProxyEnum(ProxyFactory factory) {
        this.factory = factory;
    }

    public <T> T newProxyInstance(Class<T> interfaceType, Object handler) {
        return factory.newProxyInstance(interfaceType, handler);
    }

    interface ProxyFactory {
        <T> T newProxyInstance(Class<T> interfaceType, Object handler);
    }
}
