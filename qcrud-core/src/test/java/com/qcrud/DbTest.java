package com.qcrud;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.SneakyThrows;

import javax.sql.DataSource;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Statement;
import java.util.Objects;

public class DbTest {

    private static DataSource mySqlDs;
    private static DataSource h2Ds;

    public static DataSource getMySqlConnection() {
        if (mySqlDs == null) {
            HikariConfig config = new HikariConfig();
            // src/test/resources/init_sample_db_my_sql.sql
            config.setJdbcUrl("jdbc:mysql://127.0.0.1:3306/qcrud");
            config.setUsername("root");
            config.setPassword("123456");
            config.addDataSourceProperty("cachePrepStmts", "true");
            config.addDataSourceProperty("prepStmtCacheSize", "250");
            config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
            mySqlDs = new HikariDataSource(config);
        }
        return mySqlDs;
    }

    @SneakyThrows
    public static DataSource getH2Connection() {
        if (h2Ds == null) {
            HikariConfig config = new HikariConfig();
            config.setJdbcUrl("jdbc:h2:mem:test;MODE=mysql;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE");
            h2Ds = new HikariDataSource(config);
            URI uri = Objects.requireNonNull(Thread.currentThread().getContextClassLoader().getResource("db/h2.sql")).toURI();
            try (
                Statement statement = h2Ds.getConnection().createStatement();
            ) {
                statement.execute(Files.readString(Path.of(uri)));
            }
        }
        return h2Ds;
    }
}
