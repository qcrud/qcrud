DROP TABLE IF EXISTS `person`;
CREATE TABLE `person`
(
    `id`         int GENERATED BY DEFAULT AS IDENTITY NOT NULL,
    `name`       varchar(20) NOT NULL,
    `email`      varchar(55) NULL DEFAULT NULL,
    `last_login` timestamp                                              NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
);

INSERT INTO `person` (`name`, `email`, `last_login`)
VALUES ('qcrud', 'hi@qcrud.com', '2022-08-31 20:00:00');
INSERT INTO `person` (`name`, `email`, `last_login`)
VALUES ('qcrud', 'abc@qcrud.com', '2022-08-31 20:00:00');
INSERT INTO `person` (`name`, `email`, `last_login`)
VALUES ('qcrud', 'abc@qcrud.com', '2022-08-31 20:00:00');
