SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `person`;
CREATE TABLE `person` (
`id` int(10) NOT NULL AUTO_INCREMENT,
`name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`email` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`last_login` timestamp NULL DEFAULT NULL,
PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

SET FOREIGN_KEY_CHECKS = 1;

INSERT INTO `qcrud`.`person` (`id`, `name`, `email`, `last_login`) VALUES (1, 'qcrud', 'hi@qcrud.com', '2022-08-31 20:00:00');
INSERT INTO `qcrud`.`person` (`id`, `name`, `email`, `last_login`) VALUES (2, 'qcrud', 'abc@qcrud.com', '2022-08-31 20:00:00');
INSERT INTO `qcrud`.`person` (`id`, `name`, `email`, `last_login`) VALUES (3, 'qcrud', 'abc@qcrud.com', '2022-08-31 20:00:00');
