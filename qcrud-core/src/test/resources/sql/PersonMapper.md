
# com.qcrud.records.PersonMapper
> PersonMapper 接口 SQL 内容

## selectByEmailAndName
> 根据 name 查询
```sql
select * from person where email='${email}' and name='${name}'
```

## pageByName
> 根据 name 分页查询
```sql
select * from person where name='${name}'
```

## selectByNameAndIds
> 根据 name ids 查询列表
```sql
select * from ${jt.table(tableName)} where name='${name}'
```
