
# com.qcrud.records.PersonMapper
> 行注释说明 # 表示命名空间

## selectNameById
> 行注释说明 ## 表示方法名
> 注释 2
这行不会被识别

这行不会被识别
```sql
select name from person where id=?
```

## insert
```sql
insert person (name,email,last_login) values (?,?,?)
```

## deleteById
```sql
delete from person where id=?
```

## updateEmailById
```sql
update person set email=? where id=?
```

# com.qcrud.records.UserMapper
> 第2个命名空间注释说明

## selectById
```sql
select * from user where id=?
```
