package com.qcrud.spring;

import com.qcrud.Qcrud;
import com.qcrud.core.spi.QcrudPlugin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AbstractFactoryBean;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class QcrudFactoryBean extends AbstractFactoryBean<Qcrud> {

    private DataSource dataSource;
    private boolean autoInstallPlugins = false;
    private Collection<QcrudPlugin> plugins = Collections.emptyList();

    public QcrudFactoryBean() {}

    public QcrudFactoryBean(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    protected Qcrud createInstance() throws Exception {
        final Qcrud qcrud = Qcrud.create(new SpringConnectionFactory(dataSource), null);
        if (autoInstallPlugins) {
            qcrud.installPlugins();
        }
        plugins.forEach(qcrud::installPlugin);
        return qcrud;
    }

    @Override
    public Class<Qcrud> getObjectType() {
        return Qcrud.class;
    }

    public QcrudFactoryBean setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        return this;
    }

    @Autowired(required = false)
    public QcrudFactoryBean setPlugins(Collection<QcrudPlugin> plugins) {
        this.plugins = new ArrayList<>(plugins);
        return this;
    }

    public QcrudFactoryBean setAutoInstallPlugins(boolean autoInstallPlugins) {
        this.autoInstallPlugins = autoInstallPlugins;
        return this;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        if (dataSource == null) {
            throw new IllegalStateException("'dataSource' property must be set");
        }
        super.afterPropertiesSet();
    }
}
