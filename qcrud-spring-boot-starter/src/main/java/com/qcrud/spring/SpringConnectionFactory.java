package com.qcrud.spring;

import com.qcrud.core.datasource.ConnectionFactory;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public record SpringConnectionFactory(DataSource dataSource) implements ConnectionFactory {

    @Override
    public Connection openConnection() {
        return DataSourceUtils.getConnection(dataSource);
    }

    @Override
    public void closeConnection(Connection connection) throws SQLException {
        DataSourceUtils.releaseConnection(connection, dataSource);
    }
}
