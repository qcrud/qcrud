package com.qcrud.spring;

import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnSingleCandidate;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration(proxyBeanMethods = false)
@ConditionalOnSingleCandidate(DataSource.class)
@EnableConfigurationProperties({QcrudProperties.class})
@AutoConfigureAfter({DataSourceAutoConfiguration.class})
public class QcrudAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public QcrudFactoryBean qcrudFactoryBean(DataSource dataSource) {
        return new QcrudFactoryBean(dataSource);
    }
}
