package com.qcrud.spring;

import com.qcrud.Qcrud;
import com.qcrud.core.SqlManager;
import org.springframework.beans.factory.FactoryBean;

public class MapperFactoryBean<T> implements FactoryBean<T> {
    private Class<T> mapperInterface;
    private Qcrud qcrud;

    public void setMapperInterface(Class<T> mapperInterface) {
        this.mapperInterface = mapperInterface;
    }

    public void setQcrud(Qcrud qcrud) {
        this.qcrud = qcrud;
    }

    @Override
    public T getObject() throws Exception {
        return SqlManager.getMapper(mapperInterface, qcrud);
    }

    @Override
    public Class<T> getObjectType() {
        return mapperInterface;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }

}
