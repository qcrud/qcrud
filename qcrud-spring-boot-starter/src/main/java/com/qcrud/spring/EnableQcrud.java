package com.qcrud.spring;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
@Import({QcrudScanRegistrar.class})
public @interface EnableQcrud {

    String[] basePackages() default {};

    Class<?>[] basePackageClasses() default {};
}
