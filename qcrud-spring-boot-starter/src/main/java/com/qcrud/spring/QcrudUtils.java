package com.qcrud.spring;

import com.qcrud.Qcrud;
import com.qcrud.core.Handle;
import com.qcrud.exception.QcrudException;
import com.qcrud.exception.UtilityClassException;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import java.util.HashSet;
import java.util.Set;

public class QcrudUtils {
    private static final Set<Handle> TRANSACTIONAL_HANDLES = new HashSet<>();

    private QcrudUtils() {
        throw new UtilityClassException();
    }

    public static Handle getHandle(Qcrud qcrud) {
        Handle bound = (Handle) TransactionSynchronizationManager.getResource(qcrud);
        if (bound == null) {
            bound = qcrud.open();
            if (TransactionSynchronizationManager.isSynchronizationActive()) {
                TransactionSynchronizationManager.bindResource(qcrud, bound);
                TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter(qcrud, bound));
                TRANSACTIONAL_HANDLES.add(bound);
            }
        }
        return bound;
    }

    public static void closeIfNeeded(Handle handle) {
        if (!TRANSACTIONAL_HANDLES.contains(handle)) {
            try {
                handle.close();
            } catch (Exception e) {
                throw new QcrudException(e);
            }
        }
    }

    private record TransactionSynchronizationAdapter(Qcrud db, Handle handle) implements TransactionSynchronization {

        @Override
        public void resume() {
            TransactionSynchronizationManager.bindResource(db, handle);
        }

        @Override
        public void suspend() {
            TransactionSynchronizationManager.unbindResource(db);
        }

        @Override
        public void beforeCompletion() {
            TRANSACTIONAL_HANDLES.remove(handle);
            TransactionSynchronizationManager.unbindResource(db);
        }
    }
}
